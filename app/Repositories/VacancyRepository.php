<?php


namespace App\Repositories;

use App\Models\Vacancy;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class VacancyRepository {

    private Vacancy $vacancyModel;

    public function __construct(
        Vacancy $vacancyModel
    ) {
        $this->vacancyModel = $vacancyModel;
    }

    public function create(array $data): Vacancy {
        return $this->vacancyModel
            ->create($data);
    }

    public function findById(string $id): Vacancy {
        return $this->vacancyModel::with(
            'company',
            'licenses',
            'documentations',
            'residences',
            'languages',
            'cars',
            'experiences',
            'internships',
            'documents'
        )
            ->findOrFail($id);
    }

    public function all(): Collection {
        return $this->vacancyModel
            ->orderBy('deadline', 'DESC')
            ->get();
    }

    public function update(array $data, string $id): bool {
        return $this->vacancyModel
            ->where('id', $id)
            ->update($data);
    }

    public function delete(string $id): bool {
        return $this->vacancyModel
            ->where('id', $id)
            ->delete();
    }

    public function takeLimitVacancies(int $limit): Collection {
        return $this->vacancyModel::with('company', 'licenses', 'residences')
            ->orderBy('created_at', 'DESC')
            ->take($limit)
            ->get();
    }

    public function queryBuilder(): Builder {
        return $this->vacancyModel::query();
    }

}
