<?php


namespace App\Repositories;


use App\Models\Experience;

class ExperienceRepository extends BaseOptionRepository {

    public function __construct(
        Experience $experienceModel
    ) {
        parent::__construct($experienceModel);
    }

}
