<?php


namespace App\Repositories;


use App\Models\Summary;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class SummaryRepository {

    private Summary $summaryModel;

    public function __construct(
        Summary $summaryModel
    ) {
        $this->summaryModel = $summaryModel;
    }

    public function create(array $data): Summary {
        return $this->summaryModel
            ->create($data);
    }

    public function findById(string $id): Summary {
        return $this->summaryModel
            ->findOrFail($id);
    }

    public function all() {
        return $this->summaryModel
            ->orderBy('is_selected', 'DESC')
            ->orderBy('created_at', 'DESC')
            ->get();
    }

    public function update(array $data, $id): bool {
        return $this->summaryModel
            ->where('id', $id)
            ->update($data);
    }

    public function delete(string $id): bool {
        return $this->summaryModel
            ->where('id', $id)
            ->delete();
    }

    public function takeLimitAmount(int $limit): Collection {
        return $this->summaryModel
            ->orderBy('created_at', 'DESC')
            ->take($limit)
            ->get();
    }

    public function queryBuilder(): Builder {
        return $this->summaryModel::query();
    }

}
