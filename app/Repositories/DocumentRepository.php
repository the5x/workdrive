<?php


namespace App\Repositories;


use App\Models\Document;

class DocumentRepository extends BaseOptionRepository {

    public function __construct(
        Document $documentModel
    ) {
        parent::__construct($documentModel);
    }

}
