<?php


namespace App\Repositories;

use App\Models\Company;
use Illuminate\Database\Eloquent\Collection;

class CompanyRepository {

    private Company $companyModel;

    public function __construct(
        Company $companyModel
    ) {
        $this->companyModel = $companyModel;
    }

    public function create(array $data): Company {
        return $this->companyModel
            ->create($data);
    }

    public function findById(string $id): Company {
        return $this->companyModel
            ->findOrFail($id);
    }

    public function all(): Collection {
        return $this->companyModel
            ->orderBy('title', 'ASC')
            ->get();
    }

    public function update(array $data, $id): bool {
        return $this->companyModel
            ->where('id', $id)
            ->update($data);
    }

    public function delete(string $id): bool {
        return $this->companyModel
            ->where('id', $id)
            ->delete();
    }

}
