<?php


namespace App\Repositories;


use App\Models\PasswordReset;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class PasswordResetRepository {

    private PasswordReset $passwordResetModel;

    public function __construct(
        PasswordReset $passwordResetModel
    ) {
        $this->passwordResetModel = $passwordResetModel;
    }

    public function saveToken(array $tokenData): Model {
        return $this->passwordResetModel
            ->create($tokenData);
    }

    public function searchToken(array $tokenData) {
        ['email' => $email, 'token' => $token] = $tokenData;

        return $this->passwordResetModel
            ->where(['email' => $email, 'token' => $token])
            ->first();
    }

    public function deleteTokenByEmail(string $email): bool {
        return $this->passwordResetModel
            ->where('email', $email)
            ->delete();
    }

}
