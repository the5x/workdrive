<?php


namespace App\Repositories;

use App\Models\Applicant;

class ApplicantRepository {

    private Applicant $applicantModel;

    public function __construct(
        Applicant $applicantModel
    ) {
        $this->applicantModel = $applicantModel;
    }

    public function create(array $data): Applicant {
        return $this->applicantModel
            ->create($data);
    }

    public function show(string $id): ?Applicant {
        return $this->applicantModel
            ->find($id);
    }

    public function update(array $data, string $id): bool {
        return $this->applicantModel
            ->where('id', $id)
            ->update($data);
    }

    public function updatePassword(string $email, string $password): bool {
        return $this->applicantModel
            ->where('email', $email)
            ->update(['password' => $password]);
    }

}
