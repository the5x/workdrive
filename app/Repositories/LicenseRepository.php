<?php


namespace App\Repositories;


use App\Models\License;

class LicenseRepository extends BaseOptionRepository {

    public function __construct(
        License $licenseModel
    ) {
        parent::__construct($licenseModel);
    }

}
