<?php


namespace App\Repositories;


use App\Models\Employer;
use Illuminate\Support\Facades\Auth;

class EmployerRepository {

    private Employer $employerModel;

    public function __construct(
        Employer $employerModel
    ) {
        $this->employerModel = $employerModel;
    }

    public function create(array $data): Employer {
        return $this->employerModel
            ->create($data);
    }

    public function show(string $id): ?Employer {
        return $this->employerModel::with('companies', 'vacancies')
            ->find($id);
    }

    public function update(array $data, string $id): bool {
        return $this->employerModel
            ->where('id', $id)
            ->update($data);
    }

    public function all(string $orderBy) {
        return $this->employerModel
            ->orderBy($orderBy, 'ASC')
            ->get();
    }

    public function getAllOwnersCompanies() {
        return Auth::user()->companies;
    }

    public function updatePassword(string $email, string $password): bool {
        return $this->employerModel
            ->where('email', $email)
            ->update(['password' => $password]);
    }

    public function isPremium(string $id, bool $flag): bool {
        return $this->employerModel
            ->where('id', $id)
            ->update(['is_premium' => $flag]);
    }

}
