<?php


namespace App\Services;


use App\Models\Vacancy;
use App\Repositories\VacancyRepository;
use App\Transformers\VacancyTransformer;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class VacancyService {

    private const LIMIT_SUMMARIES = 20;
    private VacancyRepository $vacancyRepository;
    private VacancyTransformer $vacancyTransformer;

    public function __construct(
        VacancyRepository $vacancyRepository,
        VacancyTransformer $vacancyTransformer
    ) {
        $this->vacancyRepository = $vacancyRepository;
        $this->vacancyTransformer = $vacancyTransformer;
    }

    public function create(array $data): Vacancy {
        $validateData = $this->vacancyTransformer->transform($data);

        return $this->vacancyRepository->create($validateData);
    }

    public function findById(string $id): Vacancy {
        return $this->vacancyRepository
            ->findById($id);
    }

    public function all(): Collection {
        return $this->vacancyRepository
            ->all();
    }

    public function update(array $data, string $id): bool {
        $validateData = $this->vacancyTransformer->updateTransform($data);

        return $this->vacancyRepository->update($validateData, $id);
    }

    public function delete(string $id): bool {
        return $this->vacancyRepository
            ->delete($id);
    }

    public function takeLimitVacancies(int $quantity): Collection {
        return $this->vacancyRepository
            ->takeLimitVacancies($quantity);
    }

    public function queryBuilder(): Builder {
        return $this->vacancyRepository
            ->queryBuilder();
    }

    public function synchronizeData(Vacancy $vacancy, array $data): void {
        $vacancy->licenses()->sync($data['license']);
        $vacancy->documentations()->sync($data['documentation']);
        $vacancy->residences()->sync($data['residence']);
        $vacancy->languages()->sync($data['language']);
        $vacancy->cars()->sync($data['car']);
        $vacancy->experiences()->sync($data['experience']);
        $vacancy->internships()->sync($data['internship']);
        $vacancy->documents()->sync($data['document']);
    }

    public function detachData(Vacancy $vacancy): void {
        $vacancy->licenses()->detach();
        $vacancy->documentations()->detach();
        $vacancy->residences()->detach();
        $vacancy->languages()->detach();
        $vacancy->cars()->detach();
        $vacancy->experiences()->detach();
        $vacancy->internships()->detach();
        $vacancy->documents()->detach();
    }

    public function getResultsBySearchQuery(): LengthAwarePaginator {
        return $this->queryBuilder()
            ->withLicenses(request('license'))
            ->withDocumentations(request('documentation'))
            ->withResidences(request('residence'))
            ->withLanguages(request('language'))
            ->withCars(request('car'))
            ->withExperiences(request('experience'))
            ->withInternships(request('internship'))
            ->withDocuments(request('document'))
            ->orderBy('deadline', 'DESC')
            ->orderBy('created_at', 'DESC')
            ->paginate(self::LIMIT_SUMMARIES);
    }

}
