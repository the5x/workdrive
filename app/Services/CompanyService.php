<?php


namespace App\Services;

use App\Models\Company;
use App\Repositories\CompanyRepository;
use App\Transformers\CompanyTransformer;
use Illuminate\Database\Eloquent\Collection;

class CompanyService {

    private CompanyRepository $companyRepository;
    private CompanyTransformer $companyTransformer;
    private UploadImageService $uploadImageService;

    public function __construct(
        CompanyRepository $companyRepository,
        CompanyTransformer $companyTransformer,
        UploadImageService $uploadImageService
    ) {
        $this->companyRepository = $companyRepository;
        $this->companyTransformer = $companyTransformer;
        $this->uploadImageService = $uploadImageService;
    }

    public function create(array $data): Company {
        $fileName = $this->getFileNameIfPhotoIsUploaded($data);
        $companyData = $this->companyTransformer->transform($data);
        $mergedData = $this->mergeCompanyData($fileName, $companyData);

        return $this->companyRepository->create($mergedData);
    }

    public function findById(string $id): Company {
        return $this->companyRepository
            ->findById($id);
    }

    public function all(): Collection {
        return $this->companyRepository
            ->all();
    }

    public function update(array $data, string $id): bool {
        $fileName = $this->getFileNameIfPhotoIsUploaded($data);
        $companyData = $this->companyTransformer->updateTransform($data);
        $mergedData = $this->mergeCompanyData($fileName, $companyData);

        return $this->companyRepository->update($mergedData, $id);
    }

    public function delete(string $id): bool {
        return $this->companyRepository->delete($id);
    }

    private function mergeCompanyData(?string $fileName, array $companyDataWithoutPhoto): array {
        return isset($fileName) ?
            array_merge($companyDataWithoutPhoto, ['photo' => $fileName]) :
            $companyDataWithoutPhoto;
    }

    private function getFileNameIfPhotoIsUploaded(array $data): ?string {
        return isset($data['photo']) ?
            $this->uploadImageService->savePhotoToDirectory($data['photo']) :
            null;
    }

}
