<?php


namespace App\Services;


use Illuminate\Database\Eloquent\Collection;

class OptionService {

    private LicenseService $licenseService;
    private DocumentService $documentService;
    private ResidenceService $residenceService;
    private LanguageService $languageService;
    private CarService $carService;
    private ExperienceService $experienceService;
    private InternshipService $internshipService;
    private DocumentationService $documentationService;

    public function __construct(
        LicenseService $licenseService,
        DocumentService $documentService,
        ResidenceService $residenceService,
        LanguageService $languageService,
        CarService $carService,
        ExperienceService $experienceService,
        InternshipService $internshipService,
        DocumentationService $documentationService
    ) {
        $this->licenseService = $licenseService;
        $this->documentService = $documentService;
        $this->residenceService = $residenceService;
        $this->languageService = $languageService;
        $this->carService = $carService;
        $this->experienceService = $experienceService;
        $this->internshipService = $internshipService;
        $this->documentationService = $documentationService;
    }

    public function getLicenses(): Collection {
        return $this->licenseService->all();
    }

    public function getDocuments(): Collection {
        return $this->documentService->all();
    }

    public function getResidences(): Collection {
        return $this->residenceService->all();
    }

    public function getLanguages(): Collection {
        return $this->languageService->all();
    }

    public function getCars(): Collection {
        return $this->carService->all();
    }

    public function getExperiences(): Collection {
        return $this->experienceService->all();
    }

    public function getInternships(): Collection {
        return $this->internshipService->all();
    }

    public function getDocumentations(): Collection {
        return $this->documentationService->all();
    }

    public function all(): array {
        $licenses = $this->getLicenses();
        $documents = $this->getDocuments();
        $residences = $this->getResidences();
        $languages = $this->getLanguages();
        $cars = $this->getCars();
        $experiences = $this->getExperiences();
        $internships = $this->getInternships();
        $documentations = $this->getDocumentations();

        return compact(
            'licenses', 'documents', 'residences', 'languages',
            'cars', 'experiences', 'internships', 'documentations',
        );
    }

}
