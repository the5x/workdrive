<?php


namespace App\Services;


use App\Repositories\LicenseRepository;

class LicenseService extends BaseOptionService {

    public function __construct(
        LicenseRepository $licenseRepository
    ) {
        parent::__construct($licenseRepository);
    }

}
