<?php


namespace App\Services;


use App\Repositories\LanguageRepository;

class LanguageService extends BaseOptionService {

    public function __construct(
        LanguageRepository $languageRepository
    ) {
        parent::__construct($languageRepository);
    }

}
