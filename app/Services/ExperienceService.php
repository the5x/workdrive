<?php


namespace App\Services;


use App\Repositories\ExperienceRepository;

class ExperienceService extends BaseOptionService {

    public function __construct(
        ExperienceRepository $experienceRepository
    ) {
        parent::__construct($experienceRepository);
    }

}
