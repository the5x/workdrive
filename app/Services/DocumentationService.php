<?php


namespace App\Services;


use App\Repositories\DocumentationRepository;

class DocumentationService extends BaseOptionService {

    public function __construct(
        DocumentationRepository $documentationRepository
    ) {
        parent::__construct($documentationRepository);
    }

}
