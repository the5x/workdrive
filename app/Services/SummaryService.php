<?php


namespace App\Services;


use App\Models\Summary;
use App\Repositories\SummaryRepository;
use App\Transformers\SummaryTransformer;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class SummaryService {

    private const LIMIT_SUMMARIES = 20;
    private SummaryRepository $summaryRepository;
    private SummaryTransformer $summaryTransformer;

    public function __construct(
        SummaryRepository $summaryRepository,
        SummaryTransformer $summaryTransformer
    ) {
        $this->summaryRepository = $summaryRepository;
        $this->summaryTransformer = $summaryTransformer;
    }

    public function create(array $data): Summary {
        $validateData = $this->summaryTransformer->transform($data);
        return $this->summaryRepository->create($validateData);
    }

    public function findById(string $id): Summary {
        return $this->summaryRepository
            ->findById($id);
    }

    public function all() {
        return $this->summaryRepository
            ->all();
    }

    public function update(array $data, string $id): bool {
        $validateData = $this->summaryTransformer->updateTransform($data);
        return $this->summaryRepository->update($validateData, $id);
    }

    public function delete(string $id): bool {
        return $this->summaryRepository
            ->delete($id);
    }

    public function takeLimitAmount(int $limit): Collection {
        return $this->summaryRepository
            ->takeLimitAmount($limit);
    }

    public function synchronizeData(Summary $summary, array $data): void {
        $summary->licenses()->sync($data['license']);
        $summary->documentations()->sync($data['documentation']);
        $summary->residences()->sync($data['residence']);
        $summary->languages()->sync($data['language']);
        $summary->cars()->sync($data['car']);
        $summary->experiences()->sync($data['experience']);
        $summary->internships()->sync($data['internship']);
        $summary->documents()->sync($data['document']);
    }

    public function detachData(Summary $summary): void {
        $summary->licenses()->detach();
        $summary->documentations()->detach();
        $summary->residences()->detach();
        $summary->languages()->detach();
        $summary->cars()->detach();
        $summary->experiences()->detach();
        $summary->internships()->detach();
        $summary->documents()->detach();
    }

    public function queryBuilder(): Builder {
        return $this->summaryRepository
            ->queryBuilder();
    }

    public function getResultsBySearchQuery(): LengthAwarePaginator {
        return $this->queryBuilder()
            ->withLicenses(request('license'))
            ->withDocumentations(request('documentation'))
            ->withResidences(request('residence'))
            ->withLanguages(request('language'))
            ->withCars(request('car'))
            ->withExperiences(request('experience'))
            ->withInternships(request('internship'))
            ->withDocuments(request('document'))
            ->orderBy('is_selected', 'DESC')
            ->orderBy('created_at', 'DESC')
            ->paginate(self::LIMIT_SUMMARIES);
    }

}
