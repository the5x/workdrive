<?php


namespace App\Services;


use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Hash;

class UserService {

    private UserRepository $userRepository;

    public function __construct(
        UserRepository $userRepository
    ) {
        $this->userRepository = $userRepository;
    }

    public function create(array $data): User {
        ['email' => $email, 'password' => $password] = $data;
        $user = ['email' => $email, 'password' => Hash::make($password)];

        return $this->userRepository
            ->create($user);
    }

    public function updatePassword(array $data): bool {
        ['email' => $email, 'password' => $password] = $data;

        return $this->userRepository
            ->updatePassword($email, Hash::make($password));
    }

}
