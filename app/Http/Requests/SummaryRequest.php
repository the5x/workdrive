<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SummaryRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array {
        return [
            'title' => 'required|string|max:190',
            'information' => 'required|string|max:500',

            'license' => "required|array|min:1",
            'license.*' => "required|string|exists:licenses,id|distinct|max:190",

            'documentation' => "required|array|min:1",
            'documentation.*' => "required|string|exists:documentations,id|distinct|max:190",

            'residence' => "required|array|min:1",
            'residence.*' => "required|string|exists:residences,id|distinct|max:190",

            'language' => "required|array|min:1",
            'language.*' => "required|string|exists:languages,id|distinct|max:190",

            'car' => "required|array|min:1",
            'car.*' => "required|string|distinct|exists:cars,id|max:190",

            'experience' => "required|string|exists:experiences,id|max:190",
            'internship' => "required|string|exists:internships,id|max:190",
            'document' => "required|string|exists:documents,id|max:190",
        ];
    }

}
