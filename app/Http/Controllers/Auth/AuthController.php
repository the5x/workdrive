<?php

namespace App\Http\Controllers\Auth;

use App\Common\UserGuard;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller {

    public function index(): View {
        return view('pages.auth.login');
    }

    public function enter(LoginRequest $request): ?RedirectResponse {
        $credentials = $request->only('email', 'password');

        if (
            Auth::guard(UserGuard::EMPLOYER)->attempt($credentials) ||
            Auth::guard(UserGuard::APPLICANT)->attempt($credentials) ||
            Auth::attempt($credentials)
        ) {
            return redirect()->route('home');
        }

        return redirect()->back();
    }

    public function logout(): RedirectResponse {
        Auth::logout();
        Session::flush();

        return redirect()->route('home');
    }

}
