<?php


namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdatePasswordRequest;
use App\Services\ApplicantService;
use App\Services\EmployerService;
use App\Services\PasswordResetService;
use App\Services\UserService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class UpdatePasswordController extends Controller {

    private EmployerService $employerService;
    private ApplicantService $applicantService;
    private UserService $userService;
    private PasswordResetService $tokenService;

    public function __construct(
        EmployerService $employerService,
        ApplicantService $applicantService,
        UserService $userService,
        PasswordResetService $tokenService
    ) {
        $this->employerService = $employerService;
        $this->applicantService = $applicantService;
        $this->userService = $userService;
        $this->tokenService = $tokenService;
    }

    public function index(string $token): View {
        return view('pages.auth.update-password', ['token' => $token]);
    }

    public function update(UpdatePasswordRequest $request): RedirectResponse {
        $data = $request->validated();

        $token = $this->tokenService->searchToken($data);

        if (isset($token)) {
            if (
                $this->applicantService->updatePassword($data) ||
                $this->employerService->updatePassword($data) ||
                $this->userService->updatePassword($data)) {
                $this->tokenService->deleteTokenByEmail($data['email']);

                return redirect()->route('login');
            }
        }

        return redirect()->back();
    }

}
