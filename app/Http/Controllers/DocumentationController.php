<?php

namespace App\Http\Controllers;

use App\Http\Requests\BaseOptionRequest;
use App\Services\DocumentationService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class DocumentationController extends Controller {

    private DocumentationService $documentationService;

    public function __construct(
        DocumentationService $documentationService
    ) {
        $this->documentationService = $documentationService;
    }

    public function index(): View {
        $documentations = $this->documentationService->all();
        return view('documentations.index', compact('documentations'));
    }

    public function create(BaseOptionRequest $request): RedirectResponse {
        $this->documentationService->create($request->validated());
        return redirect()->back();
    }

    public function delete(string $id): RedirectResponse {
        $this->documentationService->delete($id);
        return redirect()->route('documentation.index');
    }

}
