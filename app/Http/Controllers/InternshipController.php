<?php

namespace App\Http\Controllers;

use App\Http\Requests\BaseOptionRequest;
use App\Services\InternshipService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class InternshipController extends Controller {

    private InternshipService $internshipService;

    public function __construct(
        InternshipService $internshipService
    ) {
        $this->internshipService = $internshipService;
    }

    public function index(): View {
        $internships = $this->internshipService->all();
        return view('internships.index', compact('internships'));
    }

    public function create(BaseOptionRequest $request): RedirectResponse {
        $this->internshipService->create($request->validated());
        return redirect()->back();
    }

    public function delete(string $id): RedirectResponse {
        $this->internshipService->delete($id);
        return redirect()->route('internship.index');
    }

}
