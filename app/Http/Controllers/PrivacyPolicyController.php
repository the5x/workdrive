<?php

namespace App\Http\Controllers;

class PrivacyPolicyController extends Controller {

    public function index() {
        return view('pages.privacy-policy');
    }
}
