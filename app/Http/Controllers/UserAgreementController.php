<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserAgreementController extends Controller {

    public function __invoke(Request $request) {
        return view('pages.user-agreements');
    }

}
