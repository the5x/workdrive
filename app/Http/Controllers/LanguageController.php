<?php

namespace App\Http\Controllers;

use App\Http\Requests\BaseOptionRequest;
use App\Services\LanguageService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class LanguageController extends Controller {

    private LanguageService $languageService;

    public function __construct(
        LanguageService $languageService
    ) {
        $this->languageService = $languageService;
    }

    public function index(): View {
        $languages = $this->languageService->all();
        return view('languages.index', compact('languages'));
    }

    public function create(BaseOptionRequest $request): RedirectResponse {
        $this->languageService->create($request->validated());
        return redirect()->back();
    }

    public function delete(string $id): RedirectResponse {
        $this->languageService->delete($id);
        return redirect()->route('language.index');
    }

}
