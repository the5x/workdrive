<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Http\Request;

class SpeechController extends Controller {

    public function set($lang) {
        \Session::put('applocale', $lang);

        return redirect()->back();
    }

}
