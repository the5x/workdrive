<?php

namespace App\Http\Middleware;

use App\Models\Summary;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckAccessToSummary {

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next) {
        $currentSummary = Summary::findOrFail($request->route('id'));
        $currentAuthEmployer = Auth::guard('applicant')->user()->id ?? null;

        if ($currentSummary->user_id === $currentAuthEmployer || isset(Auth::user()->is_admin)) {
            return $next($request);
        }

        abort(404);
    }

}
