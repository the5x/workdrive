<?php

namespace App\Http\Middleware;

use App\Models\Company;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckAccessToCompany {

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next) {
        $currentCompany = Company::findOrFail($request->route('id'));
        $currentAuthEmployer = Auth::guard('employer')->user()->id ?? null;

        if ($currentCompany->user_id === $currentAuthEmployer || isset(Auth::user()->is_admin)) {
            return $next($request);
        }

        abort(404);
    }

}
