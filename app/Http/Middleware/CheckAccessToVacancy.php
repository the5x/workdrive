<?php

namespace App\Http\Middleware;

use App\Models\Vacancy;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckAccessToVacancy {

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next) {
        $currentVacancy = Vacancy::findOrFail($request->route('id'));
        $currentAuthEmployer = Auth::guard('employer')->user()->id ?? null;

        if ($currentVacancy->user_id === $currentAuthEmployer || isset(Auth::user()->is_admin)) {
            return $next($request);
        }

        abort(404);
    }

}
