<?php


namespace App\Builders;


use Illuminate\Database\Eloquent\Builder;

class FilterBuilder extends Builder {

    private const LICENSES = 'licenses';
    private const DOCUMENTATIONS = 'documentations';
    private const RESIDENCES = 'residences';
    private const LANGUAGES = 'languages';
    private const CARS = 'cars';
    private const EXPERIENCES = 'experiences';
    private const INTERNSHIPS = 'internships';
    private const DOCUMENTS = 'documents';

    public function withLicenses($routeSegment): FilterBuilder {
        if ($routeSegment) {
            $this->whereHas(self::LICENSES, function ($query) use ($routeSegment) {
                $query->whereIn('license_id', $routeSegment);
            });
        }

        return $this;
    }

    public function withDocumentations($routeSegment): FilterBuilder {
        if ($routeSegment) {
            $this->whereHas(self::DOCUMENTATIONS, function ($query) use ($routeSegment) {
                $query->whereIn('documentation_id', $routeSegment);
            });
        }

        return $this;
    }

    public function withResidences($routeSegment): FilterBuilder {
        if ($routeSegment) {
            $this->whereHas(self::RESIDENCES, function ($query) use ($routeSegment) {
                $query->whereIn('residence_id', $routeSegment);
            });
        }

        return $this;
    }

    public function withLanguages($routeSegment): FilterBuilder {
        if ($routeSegment) {
            $this->whereHas(self::LANGUAGES, function ($query) use ($routeSegment) {
                $query->whereIn('language_id', $routeSegment);
            });
        }

        return $this;
    }

    public function withCars($routeSegment): FilterBuilder {
        if ($routeSegment) {
            $this->whereHas(self::CARS, function ($query) use ($routeSegment) {
                $query->whereIn('car_id', $routeSegment);
            });
        }

        return $this;
    }

    public function withExperiences($routeSegment): FilterBuilder {
        if ($routeSegment) {
            $this->whereHas(self::EXPERIENCES, function ($query) use ($routeSegment) {
                $query->where('experience_id', $routeSegment);
            });
        }

        return $this;
    }

    public function withInternships($routeSegment): FilterBuilder {
        if ($routeSegment) {
            $this->whereHas(self::INTERNSHIPS, function ($query) use ($routeSegment) {
                $query->where('internship_id', $routeSegment);
            });
        }

        return $this;
    }

    public function withDocuments($routeSegment): FilterBuilder {
        if ($routeSegment) {
            $this->whereHas(self::DOCUMENTS, function ($query) use ($routeSegment) {
                $query->where('document_id', $routeSegment);
            });
        }

        return $this;
    }

}
