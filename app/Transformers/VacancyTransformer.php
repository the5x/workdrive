<?php


namespace App\Transformers;


use App\Common\UserGuard;
use Illuminate\Support\Facades\Auth;

class VacancyTransformer {

    public function transform(array $data): array {
        return [
            'user_id' => Auth::guard(UserGuard::EMPLOYER)->id(),
            'company_id' => $data['company'],
            'title' => $data['title'],
            'start_salary' => $data['start_salary'],
            'end_salary' => $data['end_salary'],
            'information' => $data['information'],
            'is_direct_employer' => isset($data['is_direct_employer']),
        ];
    }

    public function updateTransform(array $data): array {
        return [
            'title' => $data['title'],
            'information' => $data['information'],
            'start_salary' => $data['start_salary'],
            'end_salary' => $data['end_salary'],
            'is_direct_employer' => isset($data['is_direct_employer']),
        ];
    }

}
