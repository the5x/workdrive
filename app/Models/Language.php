<?php

namespace App\Models;

use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Language extends Model {

    use HasFactory, UuidTrait;

    protected $table = 'languages';
    public $incrementing = false;
    public $timestamps = false;
    protected $fillable = ['title'];

}
