<?php

namespace App\Models;

use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model {

    use HasFactory, UuidTrait;

    protected $table = 'companies';
    public $incrementing = false;
    public $timestamps = false;
    protected $fillable = ['title', 'photo', 'information', 'user_id'];

    public function vacancies() {
        return $this->hasMany(Vacancy::class, 'company_id');
    }

}
