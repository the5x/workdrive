<?php

namespace App\Models;

use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Logo extends Model {

    use HasFactory, UuidTrait;

    protected $table = 'logos';
    public $incrementing = false;
    public $timestamps = false;
    protected $fillable = ['id', 'company_id'];

    public function company(): BelongsTo {
        return $this->belongsTo(Company::class);
    }

}
