<?php

return [

    'employers' => 'Работодателям',
    'applicants' => 'Соискателям',
    'info' => 'Информация',
    'portal_rules' => 'Правила портала',


    'post_a_job' => 'Опубликовать вакансию',
    'rates_and_services' => 'Тарифы и услуги',
    'post_a_cv' => 'Опубликовать CV',
    'contacts' => 'Контакты',
    'user_agreement' => 'Пользовательское соглашение',
    'public_contract' => 'Публичный договор',
    'privacy_policy' => 'Политика конфиденциальности',
    'гules_for_posting_vacancies_and_cv' => 'Правила размещения вакансий и резюме',

];
