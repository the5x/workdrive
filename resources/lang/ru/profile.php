<?php

return [

    'update_your_profile' => 'Обновить профиль',
    'photo' => 'Фотография',
    'name' => 'Имя',
    'surname' => 'Фамилия',
    'phone_number' => 'Номер телефона',
    'update_profile' => 'Обновить профиль',

    'user_profile' => 'Профиль пользователя',
    'applicant' => 'Соискатель',
    'employer' => 'Работодатель',
    'initials' => 'Имя и фамилия',
    'no_data_available' => 'Нет доступных данных',
    'email' => 'Email',
    'created_resumes' => 'Созданные резюме',
    'created_companies' => 'Созданные компании',
    'created_vacancies' => 'Созданные вакансии',

];
