<?php

return [

    'vacancies' => 'Вакансий',
    'summaries' => 'Резюме',
    'companies' => 'Компаний',
    'title' => 'Работа найдется для каждого',

    'create_cv' => 'Создать CV',
    'create_company' => 'Создать компанию',
    'create_vacancy' => 'Создать вакансию',

];
