<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'forgot_your_password' => 'Забыли пароль',
    'login_for_users' => 'Вход для пользователей',
    'recover_password' => 'Восстановить пароль',
    'send' => 'Отправить',
    'login' => 'Логин',
    'update_password' => 'Обновить пароль',
    'update' => 'Обновить',
    'user_registration' => 'Регистрация пользователя',
    'register' => 'Зарегистрироваться',

    'employer_registration' => 'Регистрация работодателя',
    'email' => 'Эл. почта',
    'password' => 'Пароль',
    'register_as_employer' => 'Зарегистрироваться как работодатель',
    'registration_as_applicant' => 'Регистрация как соискатель',
    'registration_as_employer' => 'Регистрация как работодатель',

    'registration_the_applicant' => 'Регистрация соискателя',

];
