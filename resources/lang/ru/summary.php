<?php

return [

    'contact_person' => 'Контактное лицо',
    'basic_data' => 'Основные данные',
    'driver_license' => 'Удостоверение',
    'additional_document' => 'Дополнительные документы',
    'place_residence' => 'Страна',
    'car_type' => 'Тип авто',
    'internship' => 'Стажировка',
    'language' => 'Языки',
    'help_with_document' => 'Помощь с документами',
    'work_experience' => 'Опыт работы',
    'additional_data' => 'Дополнительные данные',
    'phone_number' => 'Номер телефона',
    'email' => 'Электронная почта',
    'no_data_available' => 'Нет данных',

    'vacancies' => 'Вакансии',
    'find_resume' => 'Найти резюме',
    'find_vacancies' => 'Найти вакансии',
    'selection' => 'Подбор',
    'summary' => 'Резюме',
    'message' => 'Просмотр резюме разрешён после покупки специального пакета. Читайте подробнее в разделе «Тарифы и услуги»',

    'create_vacancy' => 'Создать вакансию',
    'title' => 'Заголовок',
    'company' => 'Компания',
    'salary_from' => 'Заработная плата от',
    'salary_to' => 'Заработная плата до',
    'we_provide' => 'Что мы предоставляем',
    'direct_employer' => 'Прямой работодатель',

    'create_resume' => 'Создать резюме',

    'reset' => 'Сбросить',

    'update_vacancy' => 'Обновить вакансию',
    'delete_vacancy' => 'Удалить вакансию',
];
