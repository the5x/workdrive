<?php

return [

    'contact_a_description' => 'Do you have any questions? Call us!',
    'mobile_phone' => 'Mobile phone',

    'contact_b_description' => 'If you have any questions about cooperation, please contact us by email',
    'follow_us' => 'Follow us',

];
