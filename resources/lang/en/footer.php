<?php

return [

    'employers' => 'Employers',
    'applicants' => 'Applicants',
    'info' => 'Information',
    'portal_rules' => 'Portal Rules',

    'post_a_job' => 'Post a job',
    'rates_and_services' => 'Rates and Services',
    'post_a_cv' => 'Post a CV',
    'contacts' => 'Contacts',
    'user_agreement' => 'User Agreement',
    'public_contract' => 'Public contract',
    'privacy_policy' => 'Privacy Policy',
    'гules_for_posting_vacancies_and_cv' => 'Rules for posting vacancies and resumes',

];
