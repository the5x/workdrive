<?php

return [

    'contact_person' => 'Contact person',
    'basic_data' => 'Basic data',
    'driver_license' => "Driver's license",
    'additional_document' => 'Additional documents',
    'place_residence' => 'Place of residence',
    'car_type' => 'Car type',
    'internship' => 'Internship',
    'language' => 'Languages',
    'help_with_document' => 'Help with documents',
    'work_experience' => 'Work experience',
    'additional_data' => 'Additional data',
    'phone_number' => 'Phone number',
    'email' => 'Email',
    'no_data_available' => 'No data available',

    'selection' => 'Selection',
    'summary' => 'Summary',
    'message' => 'Viewing a CV is allowed after purchasing a special package. Read more in the section "Tariffs and services"',
];
