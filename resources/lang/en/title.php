<?php

return [

    'companies' => 'Companies',
    'vacancies' => 'Vacancies',
    'cv' => 'CV',
    'about_us' => 'About Us',
    'rates' => 'Rates',

];
