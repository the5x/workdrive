<?php

return [

    'update_your_profile' => 'Update your profile',
    'photo' => 'Photo',
    'name' => 'Name',
    'surname' => 'Surname',
    'phone_number' => 'Phone number',
    'update_profile' => 'Update Profile',

    'user_profile' => 'User Profile',
    'applicant' => 'Applicant',
    'employer' => 'Employer',
    'initials' => 'Firstname and Lastname',
    'no_data_available' => 'No Data Available',
    'email' => 'Email',
    'created_resumes' => 'Created Resumes',
    'created_companies' => 'Created Companies',
    'created_vacancies' => 'Created Vacancies',

];
