<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'forgot_your_password' => 'Forgot your password',
    'login_for_users' => 'Login for users',
    'recover_password' => 'Recover password',
    'send' => 'Send',
    'login' => 'Login',
    'update_password' => 'Update Password',
    'update' => 'Update',
    'user_registration' => 'User Registration',
    'register' => 'Register',
    'employer_registration' => 'Employer registration',
    'email' => 'E-mail',
    'password' => 'Password',
    'register_as_employer' => 'Register as an employer',
    'registration_as_applicant' => 'Registration as an applicant',
    'registration_as_employer' => 'Registration as an employer',
    'registration_the_applicant' => 'Registration of the applicant',

];
