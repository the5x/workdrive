<?php

return [

    'days' => 'days',

    'rate_1_subtitle' => 'The best way to find new employees',
    'rate_1_list_1' => 'Placement of a vacancy',
    'rate_1_list_2' => 'View a CV for an employer',

    'rate_2_subtitle' => 'Do you want more responses to the vacancy?',
    'rate_2_list_1' => "Highlighting the employer's vacancy in a bright color. Raising up above the rest of the vacancies.",

    'rate_3_subtitle' => 'Become a company of the Workdrive24 portal',
    'rate_3_list_1' => "Placement of the company's logo on the home page in the «Companies block»",

    'rate_4_subtitle' => 'Let the best people notice you!',
    'rate_4_list_1' => 'Placement of an advertising banner',

    'rate_5_subtitle' => 'Let the best people notice you!',
    'rate_5_list_1' => 'Placement of a vacancy',
    'rate_5_list_2' => 'View a CV for an employer',
    'rate_5_list_3' => "Placement of the company's logo on the home page in the Companies block",
    'rate_5_list_4' => 'Job allocation',

    'description_1_title' => 'View resumes',
    'description_1_subtitle' => 'Reviewing a resume for an employer.',

    'description_2_title' => 'Highlighting an ad',
    'description_2_subtitle' => 'Need employees in the company?Need employees in the company?',

    'description_3_title' => 'Place a banner',
    'description_3_subtitle' => 'Let the best get you noticed!',

    'description_4_title' => "Post your company's logo",
    'description_4_subtitle' => 'Let the best get you noticed!',

    'write_to_us' => 'Write to us',

    'reset' => 'Reset',
];
