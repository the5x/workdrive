<?php

return [

    'about' => 'About the company',
    'vacancies' => 'Company vacancies',

    'create_company' => 'Create a company',
    'company_logo' => 'Company logo',
    'photo_param' => 'Square photo, max size 700*700',
    'title' => 'Title',
    'company_information' => 'Company Information',

];
