<?php

return [

    'login' => 'Login',
    'registration' => 'Registration',
    'profile' => 'Profile',
    'logout' => 'Logout',

];
