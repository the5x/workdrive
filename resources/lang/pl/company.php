<?php

return [

    'about' => 'O firmie',
    'vacancies' => 'Oferty pracy',

    'create_company' => 'Załóż firmę',
    'company_logo' => 'Logo firmy',
    'photo_param' => 'Kwadratowe zdjęcie, maksymalny rozmiar 700 * 700',
    'title' => 'Nazwa',
    'company_information' => 'Informacje o firmie',

];
