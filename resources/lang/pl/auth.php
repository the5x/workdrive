<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'forgot_your_password' => 'Zapomniałem hasła',
    'login_for_users' => 'Zaloguj się jako użytkownik',
    'recover_password' => 'Odzyskaj hasło',
    'send' => 'Wysłać',
    'login' => 'Login',
    'update_password' => 'Zaktualizuj hasło',
    'update' => 'Zaktualizować',
    'user_registration' => 'Zarejestrować się',
    'register' => 'Zarejestrować się',

    'employer_registration' => 'Rejestracja pracodawcy',
    'email' => 'E-mail',
    'password' => 'Hasło',
    'register_as_employer' => 'Zarejestruj się jako pracodawca',
    'registration_as_applicant' => 'Rejestracja jako kandydat',
    'registration_as_employer' => 'Rejestracja jako pracodawca',

    'registration_the_applicant' => 'Rejestracja kandydata',

];
