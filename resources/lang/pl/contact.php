<?php

return [

    'contact_a_description' => 'Czy masz jakieś pytania? Zadzwoń do nas!',
    'mobile_phone' => 'Telefon komórkowy',

    'contact_b_description' => 'W sprawie współpracy napisz do nas E-mail',
    'follow_us' => 'Jesteśmy',

];
