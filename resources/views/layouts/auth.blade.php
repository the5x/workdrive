<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('includes.head')
</head>
<body>
<header class="header">
    <div class="wrapper">
        @include('includes.header')
    </div>
</header>
<main>
    @yield('content')
</main>
<footer class="footer">
    <div class="wrapper">
        @include('includes.footer')
    </div>
</footer>
</body>
</html>
