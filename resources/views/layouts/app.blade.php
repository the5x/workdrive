<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('includes.head')
</head>
<body>
<header class="header">
    <div class="wrapper">
        @include('includes.header')
    </div>
</header>
<section class="hero" style="background-image: url({{ URL::asset('assets/images/slider.jpg') }})">
    <div class="wrapper">
        @yield('hero')
    </div>
</section>
<section class="banner">
    <div class="wrapper">
        @include('includes.banners')
    </div>
</section>
<section class="partners">
    <div class="wrapper">
        <h3 class="section__title"><a class="section__title-link" href="{{ route('company.all')  }}">@lang('title.companies')</a></h3>
        @yield('partners')
    </div>
</section>
<section class="vacancy">
    <div class="wrapper">
        <h3 class="section__title"><a class="section__title-link" href="{{ route('vacancies')  }}">@lang('title.vacancies')</a></h3>
        @yield('vacancy')
    </div>
</section>
<section class="banner">
    <div class="wrapper">
        @include('includes.banners')
    </div>
</section>
<section class="summary">
    <div class="wrapper">
        <h3 class="section__title"><a class="section__title-link" href="{{ route('summaries')  }}">@lang('title.cv')</a></h3>
        @yield('summary')
    </div>
</section>
<section class="about">
    <div class="wrapper">
        @yield('about')
    </div>
</section>
<footer class="footer">
    <div class="wrapper">
        @include('includes.footer')
    </div>
</footer>
</body>
</html>
