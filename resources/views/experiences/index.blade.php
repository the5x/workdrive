@extends('layouts.admin')

@section('content')
    <div class="wrapper">
        <section class="admin">
            <a class="breadcrumbs" href="{{ route('home')  }}">Главная</a>
            <h3 class="section__title section__title_sm">Создать опыт работы</h3>

            <form action="{{ route('experience.create')  }}" method="POST">
                @csrf

                @error('title')
                <div class="message">{{ $message }}</div>
                @enderror

                <div class="admin__grid admin__grid_bottom">
                    <div class="admin__column admin__column_fl2">
                        <label for="title" class="admin__label">Название</label>
                        <input class="admin__input" type="text" name="title" value="{{ old('title') }}"/>
                    </div>
                    <div class="admin__column">
                        <button class="filter__button">Добавить</button>
                    </div>
                </div>
            </form>

            <br/><br/>

            @isset($experiences)
                <strong class="admin__subtitle">Добавлено: {{ $experiences->count()  }}</strong>
                @foreach($experiences as $experience)
                    <div class="admin__grid">
                        <div class="admin__column  admin__column_border">
                            <span class="admin__text">{{ $experience->title }}</span>
                        </div>
                        <div class="admin__column admin__column_border">
                            <form action="{{ route('experience.delete', ['id' => $experience->id]) }}" method="POST">
                                @method('DELETE')
                                @csrf
                                <button class="admin__btn-delete">Удалить</button>
                            </form>
                        </div>
                    </div>
                @endforeach
            @endisset
        </section>
    </div>
@stop
