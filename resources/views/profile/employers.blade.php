@extends('layouts.admin')

@section('content')
    <div class="wrapper">
        <section class="admin">
            <a class="breadcrumbs" href="{{ route('home')  }}">Главная</a>
            <h3 class="section__title section__title_sm">Управление премиальными аккаунтами</h3>

            @foreach($employers as $profile)
                <div class="admin__grid">
                    <div class="admin__column  admin__column_border">
                        <span class="admin__text">{{ $profile->email }}</span>
                    </div>
                    <div class="admin__column admin__column_border">

                        @if(isset(Auth::user()->is_admin))

                            @if($profile instanceof \App\Models\Employer)

                                @if($profile->is_premium)
                                    <form action="{{ route('profile.premium', ['id' => $profile->id]) }}" method="post">
                                        @method('PUT')
                                        @csrf
                                        <button class="admin__btn-delete">Выключить премиум аккаунт</button>
                                    </form>
                                @else
                                    <form action="{{ route('profile.premium', ['id' => $profile->id]) }}" method="post">
                                        @method('PUT')
                                        @csrf

                                        <button class="admin__btn-delete">Включить премиум аккаунт</button>
                                    </form>
                                @endif

                            @endif

                        @endif
                    </div>
                </div>
            @endforeach

        </section>
    </div>
@stop
