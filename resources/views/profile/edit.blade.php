@extends('layouts.admin')

@section('content')
    <div class="wrapper">
        <section class="admin">
            <a class="breadcrumbs" href="{{ route('home')  }}">Главная</a>
            <h3 class="section__title section__title_sm">@lang('profile.update_your_profile')</h3>
            <form action="{{ route('profile.update', ['id' => $profile->id])  }}" method="POST"
                  enctype="multipart/form-data">
                @method('PUT')
                @csrf
                `
                <div class="admin__grid">
                    <div class="admin__column">
                        @error('photo')
                        <div class="message">{{ $message }}</div>
                        @enderror
                        <label for="photo" class="admin__label">@lang('profile.photo')</label>
                        <input class="admin__input admin__input-file" type="file" name="photo"/>
                    </div>
                </div>

                <div class="admin__grid">
                    <div class="admin__column">
                        @error('firstname')
                        <div class="message">{{ $message }}</div>
                        @enderror
                        <label for="firstname" class="admin__label">@lang('profile.name')</label>
                        <input class="admin__input" type="text" name="firstname"
                               value="{{ old('firstname') ?: $profile->firstname }}"/>
                    </div>
                </div>

                <div class="admin__grid">
                    <div class="admin__column">
                        @error('lastname')
                        <div class="message">{{ $message }}</div>
                        @enderror
                        <label for="lastname" class="admin__label">@lang('profile.surname')</label>
                        <input class="admin__input" type="text" name="lastname"
                               value="{{ old('lastname') ?: $profile->lastname }}"/>
                    </div>
                </div>

                <div class="admin__grid">
                    <div class="admin__column">
                        @error('phone')
                        <div class="message">{{ $message }}</div>
                        @enderror
                        <label for="phone" class="admin__label">@lang('profile.phone_number')</label>
                        <input class="admin__input" type="text" name="phone"
                               value="{{ old('phone') ?: $profile->phone }}"/>
                    </div>
                </div>

                <button class="filter__button" type="submit">@lang('profile.update_profile')</button>
            </form>
        </section>
    </div>
@stop
