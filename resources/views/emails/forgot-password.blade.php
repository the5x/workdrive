<style>
    .mail__wrapper {
        height: 100%;
        padding: 20px;
        background-color: #2d3748;
        font-family: sans-serif;
    }

    .mail__box {
        position: absolute;
        max-width: 700px;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        text-align: center;
    }

    .mail__title {
        display: block;
        margin: 0 auto 30px;
        font-weight: bold;
        font-size: 24px;
        text-transform: uppercase;
        color: #ceb436;
    }

    .mail__text {
        display: block;
        margin-bottom: 30px;
        font-size: 16px;
        line-height: 24px;
        color: #cbd5e0;
        text-align: center;
    }

    .mail__button {
        display: inline-block;
        padding: 10px 20px;
        text-align: center;
        text-transform: uppercase;
        font-weight: bold;
        font-size: 14px;
        color: #ffffff;
        background-color: #ceb436;
        text-decoration: none;
    }
</style>

<div class="mail__wrapper">
    <div class="mail__box">
        <h1 class="mail__title">Сброс пароля</h1>
        <p class="mail__text">Сущность и концепция маркетинговой программы тормозит ролевой SWOT-анализ. Системный анализ индуктивно программирует эмпирический формирование имиджа, расширяя долю рынка.</p>
        <a class="mail__button" target="_blank" href="{{ route('upd_password.index', ['token' => $token]) }}">Сбросить пароль</a>
    </div>
</div>



