@extends('layouts.admin')

@section('content')
    <div class="wrapper">
        <section class="admin">
            <a class="breadcrumbs" href="{{ route('home') }}">Главная</a>
            <h3 class="section__title section__title_sm">Обновить резюме</h3>

            <form action="{{ route('summary.update', ['id' => $summary->id]) }}" method="POST">
                @method('PUT')
                @csrf

                <div class="admin__grid">
                    <div class="admin__column">
                        @error('title')
                        <div class="message">{{ $message }}</div>
                        @enderror
                        <label for="title" class="admin__label">Название</label>
                        <input class="admin__input" type="text" name="title"
                               value="{{ old('title') ?: $summary->title }}"/>
                    </div>
                </div>

                <div class="admin__grid">
                    <div class="admin__column">
                        <div class="filter__option">

                            @error('license')
                            <div class="message">{{ $message }}</div>
                            @enderror

                            <h4 class="filter__option-subtitle">Удостоверениe</h4>
                            @foreach($licenses as $license)
                                <div class="filter__option-column">
                                    <input
                                        id="{{ $license->id }}"
                                        {{ $summary->licenses->contains($license) ? 'checked' : '' }}
                                        class="filter__option-ui"
                                        type="checkbox" value="{{ $license->id }}"
                                        name="license[]">
                                    <label for="{{ $license->id }}"
                                           class="filter__option-title">{{ $license->title }}</label>
                                </div>
                            @endforeach

                        </div>
                    </div>

                    <div class="admin__column">
                        <div class="filter__option">

                            @error('documentation')
                            <div class="message">{{ $message }}</div>
                            @enderror

                            <h4 class="filter__option-subtitle">Дополнительные документы</h4>

                            @foreach($documentations as $documentation)
                                <div class="filter__option-column">
                                    <input
                                        id="{{ $documentation->id }}"
                                        {{ $summary->documentations->contains($documentation) ? 'checked' : '' }}
                                        class="filter__option-ui"
                                        type="checkbox" value="{{ $documentation->id }}"
                                        name="documentation[]">
                                    <label for="{{ $documentation->id  }}"
                                           class="filter__option-title">{{ $documentation->title  }}</label>
                                </div>
                            @endforeach

                        </div>

                    </div>

                    <div class="admin__column">
                        <div class="filter__option">
                            @error('residence')
                            <div class="message">{{ $message }}</div>
                            @enderror

                            <h4 class="filter__option-subtitle">Место жительства</h4>

                            @foreach($residences as $residence)
                                <div class="filter__option-column">
                                    <input
                                        id="{{ $residence->id }}"
                                        {{ $summary->residences->contains($residence) ? 'checked' : '' }}
                                        class="filter__option-ui" type="checkbox"
                                        value="{{ $residence->id }}"
                                        name="residence[]">
                                    <label for="{{ $residence->id }}"
                                           class="filter__option-title">{{ $residence->title }}</label>
                                </div>
                            @endforeach

                        </div>
                    </div>

                    <div class="admin__column">
                        <div class="filter__option">
                            @error('language')
                            <div class="message">{{ $message }}</div>
                            @enderror

                            <h4 class="filter__option-subtitle">Языки</h4>

                            @foreach($languages as $language)
                                <div class="filter__option-column">
                                    <input
                                        id="{{ $language->id }}"
                                        {{ $summary->languages->contains($language) ? 'checked' : '' }}
                                        class="filter__option-ui"
                                        type="checkbox" value="{{ $language->id }}"
                                        name="language[]">
                                    <label for="{{ $language->id }}"
                                           class="filter__option-title">{{ $language->title }}</label>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>

                <div class="admin__grid">
                    <div class="admin__column">
                        <div class="filter__option">
                            @error('car')
                            <div class="message">{{ $message }}</div>
                            @enderror

                            <h4 class="filter__option-subtitle">Тип авто</h4>

                            @foreach($cars as $car)
                                <div class="filter__option-column">
                                    <input
                                        id="{{ $car->id }}"
                                        {{ $summary->cars->contains($car) ? 'checked' : '' }}
                                        class="filter__option-ui"
                                        type="checkbox" value="{{ $car->id }}"
                                        name="car[]">
                                    <label for="{{ $car->id }}"
                                           class="filter__option-title">{{ $car->title }}</label>
                                </div>
                            @endforeach

                        </div>
                    </div>

                    <div class="admin__column">
                        <div class="filter__option">
                            @error('experience')
                            <div class="message">{{ $message }}</div>
                            @enderror

                            <h4 class="filter__option-subtitle">Опыт работы</h4>

                            @foreach($experiences as $experience)
                                <div class="filter__option-column">
                                    <input
                                        id="{{ $experience->id }}"
                                        {{ $summary->experiences->contains($experience) ? 'checked' : '' }}
                                        class="filter__option-ui"
                                        type="radio" value="{{ $experience->id }}"
                                        name="experience">
                                    <label for="{{ $experience->id }}"
                                           class="filter__option-title">{{ $experience->title }}</label>
                                </div>
                            @endforeach

                        </div>
                    </div>

                    <div class="admin__column">
                        <div class="filter__option">
                            @error('internship')
                            <div class="message">{{ $message }}</div>
                            @enderror

                            <h4 class="filter__option-subtitle">Стажировка</h4>

                            @foreach($internships as $internship)
                                <div class="filter__option-column">
                                    <input
                                        id="{{ $internship->id }}"
                                        {{ $summary->internships->contains($internship) ? 'checked' : '' }}
                                        class="filter__option-ui"
                                        type="radio" value="{{ $internship->id }}"
                                        name="internship">
                                    <label for="{{ $internship->id }}"
                                           class="filter__option-title">{{ $internship->title }}</label>
                                </div>
                            @endforeach

                        </div>
                    </div>

                    <div class="admin__column">
                        <div class="filter__option">
                            @error('document')
                            <div class="message">{{ $message }}</div>
                            @enderror

                            <h4 class="filter__option-subtitle">Помощь с документами</h4>

                            @foreach($documents as $document)
                                <div class="filter__option-column">
                                    <input
                                        id="{{ $document->id }}"
                                        {{ $summary->documents->contains($document) ? 'checked' : '' }}
                                        class="filter__option-ui"
                                        type="radio" value="{{ $document->id }}"
                                        name="document">
                                    <label for="{{ $document->id }}"
                                           class="filter__option-title">{{ $document->title }}</label>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>

                <div class="admin__grid">
                    <div class="admin__column">
                        @error('information')
                        <div class="message">{{ $message }}</div>
                        @enderror

                        <h4 class="filter__option-subtitle">Дополнительные данные</h4>
                        <textarea class="admin__textarea" name="information" id="" cols="30"
                                  rows="10">{{ old('information') ?: $summary->information }}</textarea>
                    </div>
                </div>

                <button class="filter__button" type="submit">Обновить резюме</button>
            </form>

            <form action="{{ route('summary.delete', ['id' => $summary->id]) }}" method="POST">
                @method('DELETE')
                @csrf

                <button class="simple__button" type="submit">Удалить резюме</button>
            </form>
        </section>
    </div>
@stop
