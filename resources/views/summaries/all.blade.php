@extends('layouts.child')

@section('content')
    <div class="wrapper">
        <a class="breadcrumbs" href="{{ route('home')  }}">Главная</a>
        <div class="cascade__grid">
            <div class="cascade__column cascade__column_mw">
                <div class="filter">
                    <h3 class="section__title section__title_sm">@lang('summary.selection')</h3>
                    <form action="{{ route('summaries') }}" method="GET">
                        @include('includes.filter')

                        <button class="filter__button" type="submit">@lang('summary.find_resume')</button>

                        @if( array_key_exists('query', parse_url( Request::fullUrl() )) )
                            <a href="{{ route('summaries') }}" class="filter__reset">@lang('summary.reset')</a>
                        @else
                            <button class="filter__reset" type="reset">@lang('summary.reset')</button>
                        @endif

                    </form>
                </div>
            </div>
            <div class="cascade__column cascade__column_ml">
                <h3 class="section__title section__title_sm">@lang('summary.summary')</h3>

                <div class="attention">
                    <h4 class="attention__title">Просмотр резюме для работадателей на платной основе</h4>
                    <p class="attention__text">@lang('summary.message')
                        <a class="attention__link" href="{{ route('rates') }}">→</a></p>
                </div>

                @if(Auth::user()->is_admin || Auth::guard('applicant')->check() || Auth::guard('employer')->user()->is_premium)
                    @foreach($summaries as $summary)
                        <div
                            class="summary__item summary__item_full {{ $summary->is_selected ? 'summary__item_yellow' : '' }}">
                            @include('includes.summary')
                        </div>
                    @endforeach

                    {{ $summaries->appends(request()->query())->onEachSide(1)->links() }}
                @endif

            </div>
        </div>

    </div>
@stop
