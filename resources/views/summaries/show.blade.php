@extends('layouts.child')

@section('content')
    <div class="wrapper">
        <a class="breadcrumbs" href="{{ route('summaries')  }}">На страницу резюме</a>
        <section class="description">
            <h3 class="section__title section__title_sm">{{ $summary->title }}

                @if(Auth::id() === $summary->user_id || isset(Auth::user()->is_admin))
                    <a class="link__ico" href="{{ route('summary.edit', ['id' => $summary->id]) }}">
                        <img class="link__ico-pic" src="{{ URL::asset('assets/images/ico-edit.svg') }}" alt=""/>
                    </a>
                @endif

            </h3>

            <div class="description__grid">
                <div class="description__column">
                    <div class="description__profile">
                        <div class="description__profile-cover">
                            @if(isset($summary->applicant->photo) && !empty($summary->applicant->photo))
                                <img class="profile__pic" src="{{ asset('uploads/' . $summary->applicant->photo )}}"
                                     alt="">
                            @else
                                <div class="profile__column profile__column_sm profile__column_border"></div>
                            @endif
                        </div>
                        <strong class="description__profile-title">@lang('summary.contact_person')</strong>

                        @if(isset($summary->applicant->firstname, $summary->applicant->lastname))
                            <span class="description__profile-name">
                                {{ $summary->applicant->firstname }} {{ $summary->applicant->lastname }}
                            </span>
                        @else
                            <em class="description__user-text">@lang('summary.no_data_available')</em>
                        @endif

                    </div>
                </div>
                <div class="description__column description__column_fl2">
                    <h5 class="description__subtitle">@lang('summary.basic_data')</h5>
                    <ul class="description__list">
                        <li class="description__item">
                            @lang('summary.driver_license'):
                            <br/>
                            @foreach($summary->licenses as $licence)
                                <span class="description__item-option">{{ $licence->title }}</span>
                            @endforeach
                        </li>
                        <li class="description__item">
                            @lang('summary.additional_document'):
                            <br/>
                            @foreach($summary->documentations as $documentation)
                                <span class="description__item-option">{{ $documentation->title }}</span>
                            @endforeach
                        </li>
                        <li class="description__item">
                            @lang('summary.place_residence'):
                            <br/>
                            @foreach($summary->residences as $residence)
                                <span class="description__item-option">{{ $residence->title }}</span>
                            @endforeach
                        </li>
                        <li class="description__item">
                            @lang('summary.car_type'):
                            <br/>
                            @foreach($summary->cars as $car)
                                <span class="description__item-option">{{ $car->title  }}</span>
                            @endforeach
                        </li>
                        <li class="description__item">
                            @lang('summary.internship'):
                            <br/>
                            @foreach($summary->internships as $internship)
                                <span class="description__item-option">{{ $internship->title }}</span>
                            @endforeach
                        </li>
                        <li class="description__item">
                            @lang('summary.language'):
                            <br/>
                            @foreach($summary->languages as $language)
                                <span class="description__item-option">{{ $language->title }}</span>
                            @endforeach
                        </li>
                        <li class="description__item">
                            @lang('summary.help_with_document'):
                            <br/>
                            @foreach($summary->documents as $document)
                                <span class="description__item-option">{{ $document->title }}</span>
                            @endforeach
                        </li>
                        <li class="description__item">
                            @lang('summary.work_experience'):
                            <br/>
                            @foreach($summary->experiences as $experience)
                                <span class="description__item-option">{{ $experience->title }}</span>
                            @endforeach
                        </li>
                    </ul>
                </div>
                <div class="description__column">
                    <div class="description__user">
                        <strong class="description__subtitle">@lang('summary.contact_person'):</strong>
                        @if(isset($summary->applicant->firstname, $summary->applicant->lastname))
                            <span class="description__user-text">
                                {{ $summary->applicant->firstname }} {{ $summary->applicant->lastname }}
                            </span>
                        @else
                            <em class="description__user-text">@lang('summary.no_data_available')</em>
                        @endif

                        <strong class="description__subtitle">@lang('summary.phone_number')</strong>

                        @if(isset( $summary->applicant->phone ))
                            <a href="tel:{{ $summary->applicant->phone }}"
                               class="description__user-text description__user-link">
                                {{ $summary->applicant->phone }}
                            </a>
                        @else
                            <em class="description__user-text">@lang('summary.no_data_available')</em>
                        @endif

                        <strong class="description__subtitle">@lang('summary.email')</strong>
                        <a href="mailto:{{ $summary->applicant->email }}"
                           class="description__user-text description__user-text_mb description__user-link">{{ $summary->applicant->email }}</a>
                    </div>
                </div>
            </div>

            <div class="description__other">
                <h3 class="description__subtitle">@lang('summary.additional_data')</h3>
                <p class="description__text">{{ $summary->information }}</p>
            </div>

        </section>
    </div>

@stop
