@extends('layouts.child')

@section('content')
    <div class="wrapper">
        <a class="breadcrumbs" href="{{ route('home')  }}">Главная</a>
        <h3 class="section__title section__title_sm">@lang('title.rates')</h3>
        <div class="rate__grid">
            <div class="rate__column">
                <div class="rate__plan-wrapper">

                    <div class="rate__wrapper">
                        <strong class="rate__plan-price">15 @lang('rate.days') — 150 <sup class="rate__plan-price-sup">BYN</sup></strong>
                    </div>

                    <em class="rate__slogan">@lang('rate.rate_1_subtitle')</em>

                    <ul class="rate__plan-list">
                        <li class="rate__plan-list-item">@lang('rate.rate_1_list_1')</li>
                        <li class="rate__plan-list-item">@lang('rate.rate_1_list_2')</li>
                    </ul>
                </div>
            </div>
            <div class="rate__column">
                <div class="rate__plan-wrapper">
                    <div class="rate__wrapper">
                        <strong class="rate__plan-price">15 @lang('rate.days') — 150 <sup class="rate__plan-price-sup">BYN</sup></strong>
                    </div>

                    <em class="rate__slogan">@lang('rate.rate_2_subtitle')</em>

                    <ul class="rate__plan-list">
                        <li class="rate__plan-list-item">@lang('rate.rate_2_list_1')</li>
                    </ul>
                </div>
            </div>
            <div class="rate__column">
                <div class="rate__plan-wrapper">

                    <div class="rate__wrapper">
                        <strong class="rate__plan-price">7 @lang('rate.days') — 210 <sup class="rate__plan-price-sup">BYN</sup></strong>
                        <strong class="rate__plan-price">15 @lang('rate.days') — 360 <sup class="rate__plan-price-sup">BYN</sup></strong>
                        <strong class="rate__plan-price">30 @lang('rate.days') — 600 <sup class="rate__plan-price-sup">BYN</sup></strong>
                    </div>

                    <em class="rate__slogan">@lang('rate.rate_3_subtitle')</em>

                    <ul class="rate__plan-list">
                        <li class="rate__plan-list-item">@lang('rate.rate_3_list_1')</li>
                    </ul>
                </div>
            </div>
            <div class="rate__column">
                <div class="rate__plan-wrapper">

                    <div class="rate__wrapper">
                        <strong class="rate__plan-price">7 @lang('rate.days') — 420 <sup class="rate__plan-price-sup">BYN</sup></strong>
                        <strong class="rate__plan-price">15 @lang('rate.days') — 750 <sup class="rate__plan-price-sup">BYN</sup></strong>
                        <strong class="rate__plan-price">30 @lang('rate.days') — 1320 <sup class="rate__plan-price-sup">BYN</sup></strong>
                    </div>

                    <em class="rate__slogan">@lang('rate.rate_4_subtitle')</em>

                    <ul class="rate__plan-list">
                        <li class="rate__plan-list-item">@lang('rate.rate_4_list_1')</li>
                    </ul>
                </div>
            </div>
            <div class="rate__column">
                <div class="rate__plan-wrapper">

                    <div class="rate__wrapper">
                        <strong class="rate__plan-price">15 @lang('rate.days') — 630 <sup class="rate__plan-price-sup">BYN</sup></strong>
                    </div>

                    <em class="rate__slogan">@lang('rate.rate_5_subtitle')</em>

                    <ul class="rate__plan-list">
                        <li class="rate__plan-list-item">@lang('rate.rate_5_list_1')</li>
                        <li class="rate__plan-list-item">@lang('rate.rate_5_list_2')</li>
                        <li class="rate__plan-list-item">@lang('rate.rate_5_list_3')</li>
                        <li class="rate__plan-list-item">@lang('rate.rate_5_list_4')</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="rate__grid">
            <div class="rate__column">
                <a href="mailto:workdrive24.com@gmail.com" class="rate__plan-btn">@lang('rate.write_to_us')</a>
            </div>
        </div>


        <br><br><br>

        <section class="rate">
            <div class="rate__grid">
                <div class="rate__column">
                    <h5 class="description__subtitle">@lang('rate.description_1_title')</h5>
                    <p class="rate__text">@lang('rate.description_1_subtitle')</p>
                </div>
                <div class="rate__column rate__column_fl3">
                    <div class="summary__item summary__item_full">
                        <div class="summary__cover summary__cover_mb">
                            <img class="summary__cover-pic" src="{{ URL::asset('assets/images/ico-user.png') }}" alt="">
                        </div>
                        <div class="summary__data">
                            <span href="#" class="summary__data-link summary__data-link_mb">Вращательный ионный...</span>
                            <div class="summary__meta">
                                <small class="summary__meta-text">Poland</small>
                            </div>
                        </div>
                        <div class="price">
                            <div class="price__column">
                                <strong class="price__number">2300</strong>
                                <small class="price__currency">BYN</small>
                            </div>
                            <div class="price__column">
                                <time class="summary__date">13.11.2020</time>
                            </div>
                        </div>
                    </div>

                    <div class="summary__item summary__item_full summary__item_opacity-50">
                        <div class="summary__cover summary__cover_mb">
                            <img class="summary__cover-pic" src="{{ URL::asset('assets/images/ico-user.png') }}" alt="">
                        </div>
                        <div class="summary__data">
                            <span href="#" class="summary__data-link summary__data-link_mb">Вращательный ионный...</span>
                            <div class="summary__meta">
                                <small class="summary__meta-text">Poland</small>
                            </div>
                        </div>
                        <div class="price">
                            <div class="price__column">
                                <strong class="price__number">2300</strong>
                                <small class="price__currency">BYN</small>
                            </div>
                            <div class="price__column">
                                <time class="summary__date">13.11.2020</time>
                            </div>
                        </div>
                    </div>

                    <div class="summary__item summary__item_full summary__item_opacity-25">
                        <div class="summary__cover summary__cover_mb">
                            <img class="summary__cover-pic" src="{{ URL::asset('assets/images/ico-user.png') }}" alt="">
                        </div>
                        <div class="summary__data">
                            <span class="summary__data-link summary__data-link_mb">Вращательный ионный...</span>
                            <div class="summary__meta">
                                <small class="summary__meta-text">Poland</small>
                            </div>
                        </div>
                        <div class="price">
                            <div class="price__column">
                                <strong class="price__number">2300</strong>
                                <small class="price__currency">BYN</small>
                            </div>
                            <div class="price__column">
                                <time class="summary__date">13.11.2020</time>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="rate__grid">
                <div class="rate__column">
                    <h5 class="description__subtitle">@lang('rate.description_2_title')</h5>
                    <p class="rate__text">@lang('rate.description_2_subtitle')</p>
                </div>
                <div class="rate__column rate__column_fl3">
                    <div class="summary__item summary__item_full summary__item_yellow">
                        <div class="summary__cover summary__cover_mb">
                            <img class="summary__cover-pic" src="{{ URL::asset('assets/images/ico-user.png') }}" alt="">
                        </div>
                        <div class="summary__data">
                            <span href="#" class="summary__data-link summary__data-link_mb">Вращательный ионный...</span>
                            <div class="summary__meta">
                                <small class="summary__meta-text">Poland</small>
                            </div>
                        </div>
                        <div class="price">
                            <div class="price__column">
                                <strong class="price__number">2300</strong>
                                <small class="price__currency">BYN</small>
                            </div>
                            <div class="price__column">
                                <time class="summary__date">13.11.2020</time>
                            </div>
                        </div>
                    </div>

                    <div class="summary__item summary__item_full summary__item_yellow">
                        <div class="summary__cover summary__cover_mb">
                            <img class="summary__cover-pic" src="{{ URL::asset('assets/images/ico-user.png') }}" alt="">
                        </div>
                        <div class="summary__data">
                            <span href="#" class="summary__data-link summary__data-link_mb">Вращательный ионный...</span>
                            <div class="summary__meta">
                                <small class="summary__meta-text">Poland</small>
                            </div>
                        </div>
                        <div class="price">
                            <div class="price__column">
                                <strong class="price__number">2300</strong>
                                <small class="price__currency">BYN</small>
                            </div>
                            <div class="price__column">
                                <time class="summary__date">13.11.2020</time>
                            </div>
                        </div>
                    </div>

                    <div class="summary__item summary__item_full summary__item_opacity-50">
                        <div class="summary__cover summary__cover_mb">
                            <img class="summary__cover-pic" src="{{ URL::asset('assets/images/ico-user.png') }}" alt="">
                        </div>
                        <div class="summary__data">
                            <span href="#" class="summary__data-link summary__data-link_mb">Вращательный ионный...</span>
                            <div class="summary__meta">
                                <small class="summary__meta-text">Poland</small>
                            </div>
                        </div>
                        <div class="price">
                            <div class="price__column">
                                <strong class="price__number">2300</strong>
                                <small class="price__currency">BYN</small>
                            </div>
                            <div class="price__column">
                                <time class="summary__date">13.11.2020</time>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="rate__grid">
                <div class="rate__column">
                    <h5 class="description__subtitle">@lang('rate.description_3_title')</h5>
                    <p class="rate__text">@lang('rate.description_3_subtitle')</p>
                </div>
                <div class="rate__column rate__column_fl3">
                    <div class="rate__banner"></div>
                </div>
            </div>

            <div class="rate__grid">
                <div class="rate__column">
                    <h5 class="description__subtitle">@lang('rate.description_4_title')</h5>
                    <p class="rate__text">@lang('rate.description_4_subtitle')</p>
                </div>
                <div class="rate__column rate__column_fl3">

                <div class="partners__list">
                    <div class="partners__item">
                        <a class="partners__item-link" href="#">
                            <img class="partners__item-link-pic" src="{{ URL::asset('assets/images/partners.png') }}" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </section>

    </div>
@stop
