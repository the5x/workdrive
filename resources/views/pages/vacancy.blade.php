@extends('layouts.child')

@section('content')
    <div class="wrapper">
        <a class="breadcrumbs" href="{{ route('vacancies')  }}">На страницу вакансий</a>
        <section class="description">
            <h3 class="section__title section__title_sm">Водитель-курьер</h3>

            <div class="description__grid">
                <div class="description__column">
                    <div class="description__company">
                        <div class="description__company-cover" style="background-image: url({{ URL::asset('assets/images/shell.jpg') }})"></div>
                        <div class="description__company-column">
                            <span class="description__company-text">Company name</span>
                            <span class="description__company-text">Poland</span>
                        </div>
                    </div>
                </div>
                <div class="description__column description__column_fl2">
                    <h5 class="description__subtitle">Требования</h5>
                    <ul class="description__list">
                        <li class="description__item">Удостоверение: A, C, E</li>
                        <li class="description__item">Дополнительные документы: Вид на жительство</li>
                        <li class="description__item">Место жительства: Russia</li>
                        <li class="description__item">Тип авто: легковой</li>
                        <li class="description__item">Стажировка: нет</li>
                        <li class="description__item">Тип авто: автобус</li>
                        <li class="description__item">Языки: русский, немецкий</li>
                        <li class="description__item">Помощь с документами: да</li>
                    </ul>
                </div>
                <div class="description__column">
                    <div class="description__user">
                        <strong class="description__subtitle">Контактное лицо</strong>
                        <span class="description__user-text">Анастасия Хамицевич</span>

                        <strong class="description__subtitle">Номер телефона</strong>
                        <a href="tel:+375291100636" class="description__user-text description__user-link">+375 (29) 110-06-36</a>

                        <strong class="description__subtitle">Электронная почта</strong>
                        <a href="mailto:company@company.com" class="description__user-text description__user-text_mb description__user-link">company@company.com</a>
                    </div>
                </div>
            </div>

            <div class="description__other">
                <h3 class="description__subtitle">Дополнительные данные</h3>
                <p class="description__text">Есть собственный автомобиль? Станьте водителем Menu. и зарабатывайте хорошие деньги! Получайте еженедельный гонорар и работайте по удобному Вам графику - начать работать можно уже завтра! Вам необходимо лишь пройти тренинг по качеству обслуживания.</p>

                <h3 class="description__subtitle">Что мы предоставляем?</h3>
                <p class="description__text">Искусство медиапланирования без оглядки на авторитеты конструктивно. Надо сказать, что план размещения экономит социальный статус. Комплексный анализ ситуации, отбрасывая подробности, позитивно усиливает традиционный канал. </p>
            </div>

        </section>
    </div>

@stop
