@extends('layouts.child')

@section('content')
    <div class="wrapper">
        <a class="breadcrumbs" href="{{ route('home')  }}">Главная</a>
        <div class="cascade__grid">
            <div class="cascade__column cascade__column_mw">
                @include('includes.sidebar')
            </div>
            <div class="cascade__column cascade__column_ml">
                <h3 class="section__title section__title_sm">Резюме</h3>

                <div class="attention">
                    <h4 class="attention__title">Просмотр резюме для работадателей на платной основе</h4>
                    <p class="attention__text">Наряду с этим, креатив по-прежнему устойчив к изменениям спроса. Рекламное сообщество нетривиально. Можно предположить, что анализ зарубежного опыта подсознательно программирует нишевый проект.</p>
                </div>

                <div class="summary__item summary__item_full">
                    <div class="summary__cover summary__cover_mb">
                        <img class="summary__cover-pic" src="{{ URL::asset('assets/images/ico-user.png') }}" alt="">
                    </div>
                    <div class="summary__data">
                        <a href="#" class="summary__data-link summary__data-link_mb">Вращательный ионный...</a>
                        <div class="summary__meta">
                            <small class="summary__meta-text">Poland</small>
                        </div>
                    </div>
                    <div class="price">
                        <div class="price__column">
                            <time class="summary__date">13.11.2020</time>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@stop
