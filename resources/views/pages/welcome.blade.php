@extends('layouts.app')

@section('hero')

    <div class="hero__box">
        <div class="hero__slider">
            @auth('applicant')
                <a href="{{ route('summary.create') }}" class="hero__link hero__link_yellow">@lang('slider.create_cv')</a>
            @endauth
            @auth('employer')
                <a href="{{ route('company.create') }}" class="hero__link hero__link_yellow">@lang('slider.create_company')</a>
                <a href="{{ route('vacancy.create') }}" class="hero__link hero__link_purple">@lang('slider.create_vacancy')</a>
            @endauth
            <h1 class="hero__title">@lang('slider.title')</h1>
        </div>

        <div class="statistics">
            <div class="statistics__column statistics__column_summary">
                <strong class="statistics__title">{{ $countSummaries }}</strong>
                <span class="statistics__subtitle">@lang('slider.summaries')</span>
            </div>
            <div class="statistics__column statistics__column_job">
                <strong class="statistics__title">{{ $countVacancies }}</strong>
                <span class="statistics__subtitle">@lang('slider.vacancies')</span>
            </div>
            <div class="statistics__column statistics__column_company">
                <strong class="statistics__title">{{ $countCompanies }}</strong>
                <span class="statistics__subtitle statistics__subtitle_mb">@lang('slider.companies')</span>
            </div>
        </div>
    </div>

@stop


@section('vacancy')
    <div class="vacancy__list">
        @foreach($vacancies as $vacancy)
            @include('includes.vacancy')
        @endforeach
    </div>
@stop


@section('summary')
    <div class="summary__list">
        @foreach($summaries as $summary)
            <div class="summary__item">
                @include('includes.summary')
            </div>
        @endforeach
    </div>
@stop

@section('partners')

    <div class="partners__list">
        @if(empty($logos))

        @else
            @foreach($logos as $logo)
                <div class="partners__item">
                    <a class="partners__item-link" href="{{ route('company.show', ['id' => $logo->company->id])  }}">
                        <img class="partners__item-link-pic" src="{{ asset('uploads/' .  $logo->company->photo) }}"
                             alt="{{ $logo->company->title }}">
                    </a>
                </div>
            @endforeach
        @endif
    </div>

@stop


@section('about')
    <h3 class="section__title">@lang('title.about_us')</h3>
    <p class="about__info">@lang('about.content')</p>
@stop
