@extends('layouts.auth')

@section('content')
    <div class="wrapper">
        <a class="breadcrumbs" href="{{ route('home')  }}">Главная</a>

        <section class="auth">
            <h3 class="section__title section__title_sm">@lang('auth.update_password')</h3>
            <br /><br />
            <form class="auth__form" action="{{ route('upd_password.update') }}" method="post">
                @csrf
                <input type="hidden" name="token" value="{{ $token }}">

                <div class="auth__ui">
                    <div class="auth__ui-column">
                        @error('email')
                        <div class="message">{{ $message }}</div>
                        @enderror

                        <label for="email" class="auth__ui-label">Email</label>
                        <input class="auth__ui-field" type="email" name="email" value="{{ old('email') }}">
                    </div>
                    <div class="auth__ui-column">
                        @error('password')
                        <div class="message">{{ $message }}</div>
                        @enderror

                        <label for="password" class="auth__ui-label">Password</label>
                        <input class="auth__ui-field" type="password" name="password">
                    </div>
                    <div class="auth__ui-column">
                        @error('password_confirmation')
                        <div class="message">{{ $message }}</div>
                        @enderror

                        <label for="password_confirmation" class="auth__ui-label">Password again</label>
                        <input class="auth__ui-field" type="password" name="password_confirmation">
                    </div>
                    <button class="filter__button">@lang('auth.update')</button>
                </div>
            </form>
        </section>
    </div>
@stop
