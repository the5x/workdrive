@extends('layouts.auth')

@section('content')
    <div class="wrapper">
        <a class="breadcrumbs" href="{{ route('home')  }}">Главная</a>

        <section class="auth">
            <h3 class="section__title section__title_sm">@lang('auth.registration_the_applicant')</h3>
            <br /><br />
            <form class="auth__form" action="{{ route('applicant.create') }}" method="post">
                @csrf
                <div class="auth__ui">
                    <div class="auth__ui-column">
                        @error('email')
                        <div class="message">{{ $message }}</div>
                        @enderror

                        <label for="email" class="auth__ui-label">@lang('auth.email')</label>
                        <input class="auth__ui-field" type="email" name="email" value="{{ old('email') }}">
                    </div>
                    <div class="auth__ui-column">
                        @error('password')
                        <div class="message">{{ $message }}</div>
                        @enderror

                        <label for="password" class="auth__ui-label">@lang('auth.password')</label>
                        <input class="auth__ui-field" type="password" name="password">
                    </div>
                    <button class="filter__button">@lang('auth.registration_the_applicant')</button>
                </div>
            </form>

            <div class="auth__choose">
                <a class="auth__choose-link auth__choose-link_yellow" href="{{ route('applicant.index') }}">
                    @lang('auth.registration_as_applicant')
                </a>
                <a class="auth__choose-link auth__choose-link_purple" href="{{ route('employer.index') }}">
                    @lang('auth.registration_as_employer')
                </a>
            </div>
        </section>
    </div>
@stop
