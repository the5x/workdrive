<div class="header__box">
    <div class="logo">
        <a class="logo__link" href="{{ route('home')  }}">
            <img class="logo__link-pic" src="{{ URL::asset('assets/images/logo.png') }}" alt="workdrive24">
        </a>
    </div>
    <div class="header__meta">
        <div>
            @if(Auth::guard('applicant')->check())

                <div class="header__meta-ui">
                    <a href="{{ route('profile.show', ['id' => Auth::guard('applicant')->user()->id]) }}"
                       class="admin__btn-delete">@lang('header.profile')
                    </a>

                    <form action="{{ route('logout')  }}" method="POST">
                        @csrf
                        <button class="admin__btn-delete">@lang('header.logout')</button>
                    </form>
                </div>

            @elseif(Auth::guard('employer')->check())

                <div class="header__meta-ui">
                    <a href="{{ route('profile.show', ['id' => Auth::guard('employer')->user()->id]) }}"
                       class="admin__btn-delete">@lang('header.profile')
                    </a>

                    <form action="{{ route('logout')  }}" method="POST">
                        @csrf
                        <button class="admin__btn-delete">@lang('header.logout')</button>
                    </form>
                </div>

            @elseif(Auth::check() && Auth::user()->is_admin)

                <div class="header__meta-ui">
                    <div class="header__nav">
                        <span class="header__nav-link">@lang('menu.managing_and_creating')</span>

                        <nav class="header__nav-wrapper">
                            <ul class="header__list">
                                <li class="header__list-item">
                                    <a class="header__list-item-link header__list-item-link_yellow"
                                       href="{{ route('logo.index') }}">@lang('menu.company_logos')</a>
                                </li>

                                <li class="header__list-item">
                                    <a class="header__list-item-link header__list-item-link_yellow"
                                       href="{{ route('company.all') }}">@lang('menu.companies')</a>
                                </li>
                            </ul>

                            <ul class="header__list">
                                <li class="header__list-item">
                                    <a class="header__list-item-link" href="{{ route('license.index') }}">@lang('menu.drivers_license')</a>
                                </li>
                                <li class="header__list-item">
                                    <a class="header__list-item-link" href="{{ route('document.index') }}">@lang('menu.additional_documents')</a>
                                </li>
                                <li class="header__list-item">
                                    <a class="header__list-item-link" href="{{ route('residence.index') }}">@lang('menu.place_of_residence')</a>
                                </li>
                                <li class="header__list-item">
                                    <a class="header__list-item-link" href="{{ route('language.index') }}">@lang('menu.languages')</a>
                                </li>
                                <li class="header__list-item">
                                    <a class="header__list-item-link" href="{{ route('car.index') }}">@lang('menu.car_type')</a>
                                </li>
                                <li class="header__list-item">
                                    <a class="header__list-item-link" href="{{ route('experience.index') }}">@lang('menu.work_experience')</a>
                                </li>
                                <li class="header__list-item">
                                    <a class="header__list-item-link"
                                       href="{{ route('internship.index') }}">@lang('menu.internship')</a>
                                </li>
                                <li class="header__list-item">
                                    <a class="header__list-item-link" href="{{ route('documentation.index') }}">@lang('menu.help_with_documents')</a>
                                </li>
                            </ul>
                        </nav>
                    </div>

                    <form action="{{ route('logout')  }}" method="POST">
                        @csrf
                        <button class="admin__btn-delete">@lang('header.logout')</button>
                    </form>
                </div>

            @elseif(Auth::check())

                <div class="header__meta-ui">
                    <form action="{{ route('logout')  }}" method="POST">
                        @csrf
                        <button class="admin__btn-delete">@lang('header.logout')</button>
                    </form>
                </div>

            @else

                <div class="header__meta-ui">
                    <a href="{{ route('login')  }}" class="admin__btn-delete">
                        @lang('header.login') / @lang('header.registration')
                    </a>
                </div>

            @endif
        </div>

        <select class="header__select" onchange="location = this.value;">
            <option class="header__select-option" {{ session()->get('applocale') === 'ru' ? 'selected': '' }} value="{{ route('set.language', 'ru') }}">ru</option>
            <option class="header__select-option" {{ session()->get('applocale') === 'en' ? 'selected': '' }} value="{{ route('set.language', 'en') }}">en</option>
            <option class="header__select-option" {{ session()->get('applocale') === 'pl' ? 'selected': '' }} value="{{ route('set.language', 'pl') }}">pl</option>
        </select>

    </div>
</div>
