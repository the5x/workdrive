<form class="search__box" action="" method="get">
    <input placeholder="Поиск по вакансиям" class="search__input" type="text">
    <button class="search__button" type="submit"><img src="{{ URL::asset('assets/images/ico-search.png') }}" alt=""></button>
</form>
