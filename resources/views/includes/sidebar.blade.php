<div class="filter">
    <h3 class="section__title section__title_sm">Подбор</h3>
    <form action="" method="">
        @csrf

        <div class="filter__option">
            <h4 class="filter__option-subtitle">Удостоверениe</h4>

            <div class="filter__option-column">
                <input class="filter__option-ui" type="checkbox" value="" name="">
                <label for="" class="filter__option-title">B</label>
            </div>
            <div class="filter__option-column">
                <input class="filter__option-ui" type="checkbox" value="" name="">
                <label for="" class="filter__option-title">C</label>
            </div>
            <div class="filter__option-column">
                <input class="filter__option-ui" type="checkbox" value="" name="">
                <label for="" class="filter__option-title">D</label>
            </div>
            <div class="filter__option-column">
                <input class="filter__option-ui" type="checkbox" value="" name="">
                <label for="" class="filter__option-title">E</label>
            </div>
        </div>

        <div class="filter__option">
            <h4 class="filter__option-subtitle">Дополнительные</h4>

            <div class="filter__option-column">
                <input class="filter__option-ui" type="checkbox" value="" name="">
                <label for="" class="filter__option-title">Код 95</label>
            </div>
            <div class="filter__option-column">
                <input class="filter__option-ui" type="checkbox" value="" name="">
                <label for="" class="filter__option-title">ADR</label>
            </div>
            <div class="filter__option-column">
                <input class="filter__option-ui" type="checkbox" value="" name="">
                <label for="" class="filter__option-title">Виза</label>
            </div>
            <div class="filter__option-column">
                <input class="filter__option-ui" type="checkbox" value="" name="">
                <label for="" class="filter__option-title">Вид на жительство</label>
            </div>
        </div>


        <div class="filter__option">
            <h4 class="filter__option-subtitle">Место жительства</h4>

            <div class="filter__option-column">
                <input class="filter__option-ui" type="checkbox" value="" name="">
                <label for="" class="filter__option-title">Беларусь</label>
            </div>
            <div class="filter__option-column">
                <input class="filter__option-ui" type="checkbox" value="" name="">
                <label for="" class="filter__option-title">Украина</label>
            </div>
            <div class="filter__option-column">
                <input class="filter__option-ui" type="checkbox" value="" name="">
                <label for="" class="filter__option-title">Латвия</label>
            </div>
            <div class="filter__option-column">
                <input class="filter__option-ui" type="checkbox" value="" name="">
                <label for="" class="filter__option-title">Литва</label>
            </div>
            <div class="filter__option-column">
                <input class="filter__option-ui" type="checkbox" value="" name="">
                <label for="" class="filter__option-title">Эстония</label>
            </div>
            <div class="filter__option-column">
                <input class="filter__option-ui" type="checkbox" value="" name="">
                <label for="" class="filter__option-title">Россия</label>
            </div>
            <div class="filter__option-column">
                <input class="filter__option-ui" type="checkbox" value="" name="">
                <label for="" class="filter__option-title">Польша</label>
            </div>
        </div>

        <div class="filter__option">
            <h4 class="filter__option-subtitle">Языки</h4>

            <div class="filter__option-column">
                <input class="filter__option-ui" type="checkbox" value="" name="">
                <label for="" class="filter__option-title">Русский</label>
            </div>
            <div class="filter__option-column">
                <input class="filter__option-ui" type="checkbox" value="" name="">
                <label for="" class="filter__option-title">Английский</label>
            </div>
            <div class="filter__option-column">
                <input class="filter__option-ui" type="checkbox" value="" name="">
                <label for="" class="filter__option-title">Польский</label>
            </div>
            <div class="filter__option-column">
                <input class="filter__option-ui" type="checkbox" value="" name="">
                <label for="" class="filter__option-title">Немецкий</label>
            </div>
        </div>

        <div class="filter__option">
            <h4 class="filter__option-subtitle">Тип авто</h4>

            <div class="filter__option-column">
                <input class="filter__option-ui" type="checkbox" value="" name="">
                <label for="" class="filter__option-title">Легковой</label>
            </div>
            <div class="filter__option-column">
                <input class="filter__option-ui" type="checkbox" value="" name="">
                <label for="" class="filter__option-title">До 3.5</label>
            </div>
            <div class="filter__option-column">
                <input class="filter__option-ui" type="checkbox" value="" name="">
                <label for="" class="filter__option-title">До 7.5</label>
            </div>
            <div class="filter__option-column">
                <input class="filter__option-ui" type="checkbox" value="" name="">
                <label for="" class="filter__option-title">Тягкач с прицепом</label>
            </div>
            <div class="filter__option-column">
                <input class="filter__option-ui" type="checkbox" value="" name="">
                <label for="" class="filter__option-title">Грузовой</label>
            </div>
            <div class="filter__option-column">
                <input class="filter__option-ui" type="checkbox" value="" name="">
                <label for="" class="filter__option-title">Грузовой с прицепом</label>
            </div>
            <div class="filter__option-column">
                <input class="filter__option-ui" type="checkbox" value="" name="">
                <label for="" class="filter__option-title">Цистерна</label>
            </div>
            <div class="filter__option-column">
                <input class="filter__option-ui" type="checkbox" value="" name="">
                <label for="" class="filter__option-title">Автобус</label>
            </div>
        </div>


        <div class="filter__option">
            <h4 class="filter__option-subtitle">Опыт работы</h4>

            <div class="filter__option-column">
                <input class="filter__option-ui" type="checkbox" value="" name="">
                <label for="" class="filter__option-title">Без опыта</label>
            </div>
            <div class="filter__option-column">
                <input class="filter__option-ui" type="checkbox" value="" name="">
                <label for="" class="filter__option-title">От 1 до 3-х лет</label>
            </div>
            <div class="filter__option-column">
                <input class="filter__option-ui" type="checkbox" value="" name="">
                <label for="" class="filter__option-title">От 3-х и выше</label>
            </div>
        </div>

        <div class="filter__option">
            <h4 class="filter__option-subtitle">Стажировка</h4>

            <div class="filter__option-column">
                <input class="filter__option-ui" type="radio" value="" name="">
                <label for="" class="filter__option-title">Да</label>
            </div>
            <div class="filter__option-column">
                <input class="filter__option-ui" type="radio" value="" name="">
                <label for="" class="filter__option-title">Нет</label>
            </div>
        </div>

        <div class="filter__option">
            <h4 class="filter__option-subtitle">Помощь с документами</h4>

            <div class="filter__option-column">
                <input class="filter__option-ui" type="radio" value="" name="">
                <label for="" class="filter__option-title">Да</label>
            </div>
            <div class="filter__option-column">
                <input class="filter__option-ui" type="radio" value="" name="">
                <label for="" class="filter__option-title">Нет</label>
            </div>
        </div>

        <button class="filter__button" type="submit">Подобрать вакансии</button>
    </form>
</div>
