<div class="summary__cover summary__cover_mb">
    <div class="vacancy__cover"
         style="background-image: url({{ asset('uploads/' .  $vacancy->company->photo) }})"></div>
</div>
<div class="summary__data">
    @if($vacancy->is_direct_employer)
        <div class="description__badge">
            @lang('summary.direct_employer')
        </div>
    @endif

    <a href="{{ route('vacancy.show', ['id' => $vacancy->id]) }}"
       class="summary__data-link summary__data-link_mb">
        {{ $vacancy->title }}
    </a>
    <div class="summary__meta">
        <div class="summary__meta-data">
            <small class="summary__meta-text">
                <a class="vacancy__meta-link"
                   href="{{ route('company.show', ['id' => $vacancy->company->id]) }}">{{ $vacancy->company->title }}</a>
            </small>
        </div>
        <div class="summary__meta-data">
            @foreach($vacancy->residences as $residence)
                <small class="summary__meta-text">
                    {{ $residence->title }}
                </small>
            @endforeach
        </div>
    </div>
</div>
<div class="price">
    <div class="price__column">
        @include('includes.start-end-salary')
    </div>
    <div class="price__column">
        <time class="summary__date">{{ date('d-m-Y', strtotime($vacancy->created_at)) }}</time>
    </div>
</div>
