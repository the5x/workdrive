<div class="footer__list">
        <div class="footer__item">
            <h5 class="footer__subtitle">@lang('footer.employers')</h5>
            <ul class="footer__nav">
                <li class="footer__nav-item"><a class="footer__nav-item-link" href="{{ route('vacancy.create') }}">@lang('footer.post_a_job')</a></li>
                <li class="footer__nav-item"><a class="footer__nav-item-link" href="{{ route('rates') }}">@lang('footer.rates_and_services')</a></li>
            </ul>
        </div>

    @if(Auth::guard('applicant')->check())
        <div class="footer__item">
            <h5 class="footer__subtitle">@lang('footer.applicants')</h5>
            <ul class="footer__nav">
                <li class="footer__nav-item"><a class="footer__nav-item-link" href="{{ route('summary.create') }}">@lang('footer.post_a_cv')</a></li>
            </ul>
        </div>
    @endif

    <div class="footer__item">
        <h5 class="footer__subtitle">@lang('footer.info')</h5>
        <ul class="footer__nav">
            <li class="footer__nav-item"><a class="footer__nav-item-link" href="{{ route('contacts') }}">@lang('footer.contacts')</a></li>
        </ul>
    </div>

    <div class="footer__item">
        <h5 class="footer__subtitle">@lang('footer.portal_rules')</h5>
        <ul class="footer__nav">
            <li class="footer__nav-item"><a class="footer__nav-item-link" href="{{ route('user.agreement') }}">@lang('footer.portal_rules')</a></li>
            <li class="footer__nav-item"><a class="footer__nav-item-link" href="{{ route('public.index') }}">@lang('footer.public_contract')</a></li>
            <li class="footer__nav-item"><a class="footer__nav-item-link" href="{{ route('privacy.index') }}">@lang('footer.privacy_policy')</a></li>
            <li class="footer__nav-item"><a class="footer__nav-item-link" href="{{ route('resume.index') }}">@lang('footer.гules_for_posting_vacancies_and_cv')</a></li>
        </ul>
    </div>
</div>
<div class="copy">
    <a class="copy__link" href="https://nastarte.by/" target="_blank">Дизайн и разработка — NaStarte</a>
</div>
