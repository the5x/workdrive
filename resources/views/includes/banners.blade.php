@php
    $banners = [
        ['link' => 'https://workdrive24.com/', 'image'=> 'default.jpg'],
    ];

    $randomBanner = empty($banners) ? [] : $banners[array_rand($banners, 1)];
@endphp

@if(empty($randomBanner))
    <a href="{{ route('rates') }}">
        <div class="rate__banner"></div>
    </a>
@else
    <a target="_blank" class="banner__link" href="{{ $randomBanner['link'] }}">
        <img class="banner__link-images" src="{{ URL::asset('assets/banners/' . $randomBanner['image']) }}" alt="">
    </a>
@endempty
