@extends('layouts.admin')

@section('content')
    <div class="wrapper">
        <section class="admin">
            <a class="breadcrumbs" href="{{ route('home') }}">Главная</a>
            <h3 class="section__title section__title_sm">@lang('company.create_company')</h3>

            <form action="{{ route('company.save') }}" method="POST" enctype="multipart/form-data">
                @csrf

                <div class="admin__grid">
                    <div class="admin__column">
                        @error('photo')
                        <div class="message">{{ $message }}</div>
                        @enderror
                        <label for="title" class="admin__label">@lang('company.company_logo')</label>
                        <small>@lang('company.photo_param')</small>
                        <input class="admin__input admin__input-file" type="file" name="photo"
                               value="{{ old('photo') }}"/>
                    </div>
                </div>

                <div class="admin__grid">
                    <div class="admin__column">
                        @error('title')
                        <div class="message">{{ $message }}</div>
                        @enderror
                        <label for="title" class="admin__label">@lang('company.title')</label>
                        <input class="admin__input" type="text" name="title" value="{{ old('title') }}"/>
                    </div>
                </div>

                <div class="admin__grid">
                    <div class="admin__column">
                        @error('information')
                        <div class="message">{{ $message }}</div>
                        @enderror

                        <label for="title" class="admin__label">@lang('company.company_information')</label>
                        <textarea class="admin__textarea" name="information" cols="30"
                                  rows="10">{{ old('information') }}</textarea>
                    </div>
                </div>

                <button class="filter__button" type="submit">@lang('company.create_company')</button>
            </form>
        </section>
    </div>
@stop
