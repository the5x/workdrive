@extends('layouts.admin')

@section('content')
    <div class="wrapper">
        <section class="admin">
            <a class="breadcrumbs" href="{{ route('home') }}">Главная</a>
            <h3 class="section__title section__title_sm">Создать компанию</h3>

            <form action="{{ route('company.update', ['id' => $company->id]) }}" method="POST"
                  enctype="multipart/form-data">
                @method('PUT')
                @csrf

                <div class="admin__grid">
                    <div class="admin__column">
                        @error('photo')
                        <div class="message">{{ $message }}</div>
                        @enderror
                        <label for="title" class="admin__label">Логотип компании</label>
                        <small class="admin__small">Квадратное фото, размером максимум 700x700</small>
                        <input class="admin__input admin__input-file" type="file" name="photo"
                               value="{{ old('photo') }}"/>
                    </div>
                </div>

                <div class="admin__grid">
                    <div class="admin__column">
                        @error('title')
                        <div class="message">{{ $message }}</div>
                        @enderror
                        <label for="title" class="admin__label">Название</label>
                        <input class="admin__input" type="text" name="title"
                               value="{{ $company->title ?: old('title') }}"/>
                    </div>
                </div>

                <div class="admin__grid">
                    <div class="admin__column">
                        @error('information')
                        <div class="message">{{ $message }}</div>
                        @enderror

                        <h4 class="filter__option-subtitle">Информация о компании</h4>
                        <textarea class="admin__textarea" name="information" cols="30"
                                  rows="10">{{ $company->information ?: old('information') }}</textarea>
                    </div>
                </div>

                <button class="filter__button" type="submit">Обновить компанию</button>
            </form>

            <br>
            <br>

            <form action="{{ route('company.delete', ['id' => $company->id]) }}" method="POST">
                @method('DELETE')
                @csrf
                <button class="admin__btn-delete">Удалить</button>
            </form>
        </section>
    </div>
@stop
