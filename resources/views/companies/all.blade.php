@extends('layouts.admin')

@section('content')
    <div class="wrapper">
        <section class="admin">
            <a class="breadcrumbs" href="{{ route('home')  }}">Главная</a>
            <h3 class="section__title section__title_sm">Все компании</h3>

            @foreach($companies as $company)
                <div class="admin__grid">
                    <div class="admin__column  admin__column_border">
                        <a class="admin__text"
                           href="{{ route('company.show', ['id' => $company->id]) }}">{{ $company->title }}</a>
                    </div>
                    <div class="admin__column admin__column_border">
                        @if(isset(Auth::user()->is_admin))
                            <form action="{{ route('company.delete', ['id' => $company->id]) }}" method="POST">
                                @method('DELETE')
                                @csrf
                                <button class="admin__btn-delete">Удалить</button>
                            </form>
                        @endif
                    </div>
                </div>
            @endforeach

        </section>
    </div>
@stop
