@extends('layouts.child')

@section('content')
    <div class="wrapper">
        <a class="breadcrumbs" href="{{ route('company.all')  }}">Компании</a>
        <section class="description">
            <h3 class="section__title section__title_sm">{{ $company->title }}

                @if(Auth::id() === $company->user_id || isset(Auth::user()->is_admin))
                    <a class="link__ico" href="{{ route('company.edit', ['id' => $company->id]) }}">
                        <img class="link__ico-pic" src="{{ URL::asset('assets/images/ico-edit.svg') }}" alt=""/>
                    </a>
                @endif
            </h3>

            <div class="description__grid">
                <div class="description__column">
                    <div class="description__profile">
                        <div class="description__profile-cover"
                             style="background-image: url({{ asset('uploads/' . $company->photo )}})"></div>
                    </div>
                </div>
                <div class="description__column description__column_fl2">
                    <h5 class="description__subtitle description__subtitle_mb">@lang('company.about')</h5>
                    <p class="company__info">{{ $company->information }}</p>
                </div>
                <div class="description__column"></div>
            </div>

            <div class="description__other">
                <h5 class="description__subtitle description__subtitle_mb">@lang('company.vacancies')</h5>
                <div class="summary__list">
                    @foreach($company->vacancies as $vacancy)
                        <div
                            class="summary__item {{ $vacancy->is_selected ? 'summary__item_yellow' : '' }}">
                            @include('includes.vacancy_small')
                        </div>
                    @endforeach
                </div>
            </div>

        </section>
    </div>

@stop
