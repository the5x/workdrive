@extends('layouts.child')

@section('content')
    <div class="wrapper">
        <a class="breadcrumbs" href="{{ route('home')  }}">Главная</a>
        <div class="cascade__grid">
            <div class="cascade__column cascade__column_mw">
                <div class="filter">
                    <h3 class="section__title section__title_sm">@lang('summary.selection')</h3>
                    <form action="{{ route('vacancies') }}" method="GET">
                        @include('includes.filter')
                        <button class="filter__button" type="submit">@lang('summary.find_vacancies')</button>

                        @if( array_key_exists('query', parse_url( Request::fullUrl() )) )
                            <a href="{{ route('vacancies') }}" class="filter__reset">@lang('summary.reset')</a>
                        @else
                            <button class="filter__reset" type="reset">@lang('summary.reset')</button>
                        @endif

                    </form>
                </div>
            </div>
            <div class="cascade__column cascade__column_ml">
                <h3 class="section__title section__title_sm">@lang('summary.vacancies')</h3>

                @foreach($vacancies as $vacancy)
                    <div
                        class="summary__item summary__item_full {{ \Carbon\Carbon::now() < $vacancy->deadline ? 'summary__item_yellow' : '' }}">
                        @include('includes.vacancy_small')
                    </div>
                @endforeach

                {{ $vacancies->appends(request()->query())->onEachSide(1)->links() }}

            </div>
        </div>

    </div>
@stop
