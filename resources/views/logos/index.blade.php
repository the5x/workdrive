@extends('layouts.admin')

@section('content')
    <div class="wrapper">
        <section class="admin">
            <a class="breadcrumbs" href="{{ route('home')  }}">Главная</a>
            <h3 class="section__title section__title_sm">Создать логотип компании</h3>

            <form action="{{ route('logo.save') }}" method="POST">
                @csrf

                @error('title')
                <div class="message">{{ $message }}</div>
                @enderror

                @foreach($companies as $company)
                    <div class="filter__option-column">
                        <input class="filter__option-ui"
                               id="{{ $company->id }}"
                               value="{{ $company->id }}"
                               name="company[]"
                               {{ in_array($company->id, array_column($logos->toArray(), 'company_id')) ? 'checked' : '' }}
                               type="checkbox">

                        <label class="filter__option-title"
                               for="{{ $company->id }}">{{ $company->title }}</label>
                    </div>
                @endforeach

                <div class="admin__column">
                    <button class="filter__button">Сохранить</button>
                </div>
            </form>

        </section>
    </div>
@stop
