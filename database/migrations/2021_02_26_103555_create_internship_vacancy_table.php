<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInternshipVacancyTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('internship_vacancy', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('internship_id');
            $table->uuid('vacancy_id');

            $table->foreign('internship_id')->references('id')->on('internships')->onDelete('cascade');
            $table->foreign('vacancy_id')->references('id')->on('vacancies')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('internship_vacancy');
    }

}
