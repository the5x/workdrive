<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResidenceVacancyTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('residence_vacancy', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('residence_id');
            $table->uuid('vacancy_id');

            $table->foreign('residence_id')->references('id')->on('residences')->onDelete('cascade');
            $table->foreign('vacancy_id')->references('id')->on('vacancies')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('residence_vacancy');
    }

}
