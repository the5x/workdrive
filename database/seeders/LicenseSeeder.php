<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class LicenseSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('licenses')->delete();

        $data = [
            ['id' => (string)Str::uuid(), 'title' => 'B'],
            ['id' => (string)Str::uuid(), 'title' => 'C'],
            ['id' => (string)Str::uuid(), 'title' => 'D'],
            ['id' => (string)Str::uuid(), 'title' => 'E'],
        ];

        DB::table('licenses')->insert($data);
    }

}
