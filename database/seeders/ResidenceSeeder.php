<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ResidenceSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('residences')->delete();

        $data = [
            ['id' => (string)Str::uuid(), 'title' => 'Беларусь'],
            ['id' => (string)Str::uuid(), 'title' => 'Украина'],
            ['id' => (string)Str::uuid(), 'title' => 'Латвия'],
            ['id' => (string)Str::uuid(), 'title' => 'Литва'],
            ['id' => (string)Str::uuid(), 'title' => 'Эстония'],
            ['id' => (string)Str::uuid(), 'title' => 'Россия'],
            ['id' => (string)Str::uuid(), 'title' => 'Польша'],
        ];

        DB::table('residences')->insert($data);
    }

}
