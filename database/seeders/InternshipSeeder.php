<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class InternshipSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('internships')->delete();

        $data = [
            ['id' => (string)Str::uuid(), 'title' => 'Да'],
            ['id' => (string)Str::uuid(), 'title' => 'Нет'],
        ];

        DB::table('internships')->insert($data);
    }

}
