<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ExperienceSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('experiences')->delete();

        $data = [
            ['id' => (string)Str::uuid(), 'title' => 'Без опыта'],
            ['id' => (string)Str::uuid(), 'title' => 'От 1 до 3-х лет'],
            ['id' => (string)Str::uuid(), 'title' => 'От 3-х и выше'],
        ];

        DB::table('experiences')->insert($data);
    }

}
