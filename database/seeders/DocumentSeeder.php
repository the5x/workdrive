<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class DocumentSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('documents')->delete();

        $data = [
            ['id' => (string)Str::uuid(), 'title' => 'Да'],
            ['id' => (string)Str::uuid(), 'title' => 'Нет'],
        ];

        DB::table('documents')->insert($data);
    }

}
