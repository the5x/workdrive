<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class DocumentationSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('documentations')->delete();

        $data = [
            ['id' => (string)Str::uuid(), 'title' => 'Код 95'],
            ['id' => (string)Str::uuid(), 'title' => 'ADR'],
            ['id' => (string)Str::uuid(), 'title' => 'Виза'],
            ['id' => (string)Str::uuid(), 'title' => 'Вид на жительство'],
        ];

        DB::table('documentations')->insert($data);
    }

}
