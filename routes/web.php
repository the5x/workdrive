<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Auth\EmployerController;
use App\Http\Controllers\Auth\UserController;
use App\Http\Controllers\Auth\ApplicantController;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\UpdatePasswordController;
use App\Http\Controllers\CarController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\DocumentController;
use App\Http\Controllers\DocumentationController;
use App\Http\Controllers\ExperienceController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\InternshipController;
use App\Http\Controllers\PrivacyPolicyController;
use App\Http\Controllers\PublicContractController;
use App\Http\Controllers\LanguageController;
use App\Http\Controllers\LicenseController;
use App\Http\Controllers\LogoController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\VacancyController;
use App\Http\Controllers\RateController;
use App\Http\Controllers\ResidenceController;
use App\Http\Controllers\SpeechController;
use App\Http\Controllers\SummaryController;
use App\Http\Controllers\UserAgreementController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
k| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['switch.language'])->group(function () {

    // Register
    Route::group(['prefix' => 'register', 'middleware' => ['guest:web,applicant,employer']], function () {
        Route::get('employer', [EmployerController::class, 'index'])->name('employer.index');
        Route::post('employer', [EmployerController::class, 'create'])->name('employer.create');

        Route::get('applicant', [ApplicantController::class, 'index'])->name('applicant.index');
        Route::post('applicant', [ApplicantController::class, 'create'])->name('applicant.create');

        Route::get('user', [UserController::class, 'index'])->name('user.index');
        Route::post('user', [UserController::class, 'create'])->name('user.create');
    });

    // Auth
    Route::group(['prefix' => 'login', 'middleware' => ['guest:web,applicant,employer']], function () {
        Route::get('/', [AuthController::class, 'index'])->name('login');
        Route::post('/', [AuthController::class, 'enter'])->name('login.enter');
    });

    // Password
    Route::group(['prefix' => 'password', 'middleware' => ['guest:web,applicant,employer']], function () {
        Route::get('forgot', [ForgotPasswordController::class, 'index'])->name('forgot.index');
        Route::post('forgot', [ForgotPasswordController::class, 'sendTokenToEmail'])->name('forgot.send');

        Route::get('update/{token}', [UpdatePasswordController::class, 'index'])->name('upd_password.index');
        Route::post('update', [UpdatePasswordController::class, 'update'])->name('upd_password.update');
    });

    //Drivers license
    Route::group(['prefix' => 'licenses', 'middleware' => ['admin']], function () {
        Route::get('/', [LicenseController::class, 'index'])->name('license.index');
        Route::post('/', [LicenseController::class, 'create'])->name('license.create');
        Route::delete('{id}', [LicenseController::class, 'delete'])->name('license.delete');
    });

    //Documents
    Route::group(['prefix' => 'documents', 'middleware' => ['admin']], function () {
        Route::get('/', [DocumentController::class, 'index'])->name('document.index');
        Route::post('create', [DocumentController::class, 'create'])->name('document.create');
        Route::delete('{id}', [DocumentController::class, 'delete'])->name('document.delete');
    });

    //Residence
    Route::group(['prefix' => 'residences', 'middleware' => ['admin']], function () {
        Route::get('/', [ResidenceController::class, 'index'])->name('residence.index');
        Route::post('/', [ResidenceController::class, 'create'])->name('residence.create');
        Route::delete('{id}', [ResidenceController::class, 'delete'])->name('residence.delete');
    });

    //Languages
    Route::group(['prefix' => 'languages', 'middleware' => ['admin']], function () {
        Route::get('/', [LanguageController::class, 'index'])->name('language.index');
        Route::post('/', [LanguageController::class, 'create'])->name('language.create');
        Route::delete('{id}', [LanguageController::class, 'delete'])->name('language.delete');
    });

    //Cars
    Route::group(['prefix' => 'cars', 'middleware' => ['admin']], function () {
        Route::get('/', [CarController::class, 'index'])->name('car.index');
        Route::post('/', [CarController::class, 'create'])->name('car.create');
        Route::delete('{id}', [CarController::class, 'delete'])->name('car.delete');
    });

    //Experiences
    Route::group(['prefix' => 'experiences', 'middleware' => ['admin']], function () {
        Route::get('/', [ExperienceController::class, 'index'])->name('experience.index');
        Route::post('/', [ExperienceController::class, 'create'])->name('experience.create');
        Route::delete('{id}', [ExperienceController::class, 'delete'])->name('experience.delete');
    });

    //Internships
    Route::group(['prefix' => 'internships', 'middleware' => ['admin']], function () {
        Route::get('/', [InternshipController::class, 'index'])->name('internship.index');
        Route::post('/', [InternshipController::class, 'create'])->name('internship.create');
        Route::delete('{id}', [InternshipController::class, 'delete'])->name('internship.delete');
    });

    //Documentations
    Route::group(['prefix' => 'documentations', 'middleware' => ['admin']], function () {
        Route::get('/', [DocumentationController::class, 'index'])->name('documentation.index');
        Route::post('/', [DocumentationController::class, 'create'])->name('documentation.create');
        Route::delete('{id}', [DocumentationController::class, 'delete'])->name('documentation.delete');
    });

    //Summary (applicant)
    Route::group(['prefix' => 'summaries', 'middleware' => ['auth:applicant']], function () {
        Route::get('create', [SummaryController::class, 'create'])->name('summary.create');
        Route::post('create', [SummaryController::class, 'save'])->name('summary.save');
    });

    //Summary (admin, applicant)
    Route::group(['prefix' => 'summaries', 'middleware' => ['access.summary']], function () {
        Route::get('{id}/update', [SummaryController::class, 'edit'])->name('summary.edit');
        Route::put('{id}/update', [SummaryController::class, 'update'])->name('summary.update');
        Route::delete('{id}', [SummaryController::class, 'delete'])->name('summary.delete');
    });

    //Summary
    Route::group(['prefix' => 'summaries', 'middleware' => ['auth:web,applicant,employer']], function () {
        Route::get('/', [SummaryController::class, 'index'])->name('summaries');
        Route::get('{id}', [SummaryController::class, 'show'])->name('summary.show');
    });

    //Vacancies (employer)
    Route::group(['prefix' => 'vacancies', 'middleware' => ['auth:employer']], function () {
        Route::get('create', [VacancyController::class, 'create'])->name('vacancy.create');
        Route::post('create', [VacancyController::class, 'save'])->name('vacancy.save');
    });

    //Vacancies (admin, employer)
    Route::group(['prefix' => 'vacancies', 'middleware' => ['access.vacancy']], function () {
        Route::get('{id}/update', [VacancyController::class, 'edit'])->name('vacancy.edit');
        Route::put('{id}/update', [VacancyController::class, 'update'])->name('vacancy.update');
        Route::delete('{id}', [VacancyController::class, 'delete'])->name('vacancy.delete');
    });

    //Vacancies (admin)
    Route::group(['prefix' => 'vacancies', 'middleware' => ['admin']], function () {
        Route::put('{id}/premium', [VacancyController::class, 'premium'])->name('vacancy.premium');
        Route::put('{id}/free', [VacancyController::class, 'free'])->name('vacancy.free');
    });

    //Vacancies
    Route::group(['prefix' => 'vacancies', 'middleware' => ['auth:web,employer,applicant']], function () {
        Route::get('/', [VacancyController::class, 'index'])->name('vacancies');
        Route::get('{id}', [VacancyController::class, 'show'])->name('vacancy.show');
    });

    //Companies (employers)
    Route::group(['prefix' => 'companies', 'middleware' => ['auth:employer']], function () {
        Route::get('create', [CompanyController::class, 'create'])->name('company.create');
        Route::post('create', [CompanyController::class, 'save'])->name('company.save');
    });

    //Companies (employer)
    Route::group(['prefix' => 'companies', 'middleware' => ['access.company']], function () {
        Route::get('{id}/update', [CompanyController::class, 'edit'])->name('company.edit');
        Route::put('{id}/update', [CompanyController::class, 'update'])->name('company.update');
    });

    //Companies (web, employer, applicant)
    Route::group(['prefix' => 'companies', 'middleware' => ['auth:web,employer,applicant']], function () {
        Route::get('/', [CompanyController::class, 'all'])->name('company.all');
        Route::get('{id}', [CompanyController::class, 'show'])->name('company.show');
    });

    //Companies (admin)
    Route::group(['prefix' => 'companies', 'middleware' => ['access.company']], function () {
        Route::delete('{id}', [CompanyController::class, 'delete'])->name('company.delete');
    });

    //Employers (admin)
    Route::group(['prefix' => 'employers', 'middleware' => ['admin']], function () {
        Route::get('/', [EmployerController::class, 'all'])->name('employers.all');
    });

    //Profile
    Route::group(['prefix' => 'profile', 'middleware' => ['access.profile:employer,applicant']], function () {
        Route::get('{id}', [ProfileController::class, 'show'])->name('profile.show');
        Route::get('{id}/update', [ProfileController::class, 'edit'])->name('profile.edit');
        Route::put('{id}/update', [ProfileController::class, 'update'])->name('profile.update');

        Route::put('{id}/premium', [ProfileController::class, 'premium'])->name('profile.premium');
    });

    //Logos (admin)
    Route::group(['prefix' => 'logos', 'middleware' => 'admin'], function () {
        Route::get('/', [LogoController::class, 'index'])->name('logo.index');
        Route::post('/', [LogoController::class, 'save'])->name('logo.save');
    });

    //Logout (web, applicant, employer)
    Route::group(['prefix' => 'logout', ['auth:web,applicant,employer']], function () {
        Route::get('/', function () {
            abort(404);
        });
        Route::post('/', [AuthController::class, 'logout'])->name('logout');
    });

    //User Agreement
    Route::get('/user-agreement', UserAgreementController::class)->name('user.agreement');

    //Public contract
    Route::get('/public-contract', [PublicContractController::class, 'index'])->name('public.index');

    //Privacy policy
    Route::get('/privacy-policy', [PrivacyPolicyController::class, 'index'])->name('privacy.index');

    //Resume placement rules
    Route::get('/resume-placement-rules', [PrivacyPolicyController::class, 'index'])->name('resume.index');

    //Change language
    Route::get('language/{lang}', [SpeechController::class, 'set'])->name('set.language');

    //Home
    Route::get('/', [HomeController::class, 'index'])->name('home');

    //Rates
    Route::get('/rates', [RateController::class, 'index'])->name('rates');

    //Contacts
    Route::get('/contacts', [ContactController::class, 'index'])->name('contacts');
});
